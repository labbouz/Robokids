<?php

use Illuminate\Database\Seeder;

use App\Module;

class ModulesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Let's truncate our existing records to start from scratch.
        //Module::truncate();


        Module::create([
            'titre' => 'Informatique',
            'code' => 'info'
        ]);

        Module::create([
            'titre' => 'Robotique',
            'code' => 'robo'
        ]);
    }
}
