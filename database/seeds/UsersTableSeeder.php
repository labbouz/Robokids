<?php

use Illuminate\Database\Seeder;

use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Let's clear the users table first
        User::truncate();

        $password = Hash::make('ccxccb01');

        User::create([
            'name' => 'Administrateur',
            'email' => 'admin@robokids.tn',
            'password' => $password,
        ]);
    }
}
