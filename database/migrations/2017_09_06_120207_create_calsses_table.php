<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCalssesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('calsses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('formation_id')->unsigned();
            $table->foreign('formation_id')->references('id')->on('formations');
            $table->integer('horaire_id')->unsigned();
            $table->foreign('horaire_id')->references('id')->on('horaires');
            $table->integer('salle_id')->unsigned();
            $table->foreign('salle_id')->references('id')->on('salles');
            $table->integer('groupe_id')->unsigned();
            $table->foreign('groupe_id')->references('id')->on('groupes');
            $table->integer('max_childrens')->default(10);
            $table->timestamps();

            $table->unique(array('formation_id', 'groupe_id'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('calsses');
    }
}
