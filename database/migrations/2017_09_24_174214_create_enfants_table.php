<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEnfantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('enfants', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nom');
            $table->string('prenom');
            $table->date('date_naissance');
            $table->string('genre');
            $table->string('ecole');

            $table->string('photo')->nullable();

            $table->string('adresse')->nullable();
            $table->string('ville')->nullable();
            $table->string('code_postal')->nullable();

            $table->string('nom_pere')->nullable();
            $table->string('prenom_pere')->nullable();
            $table->string('telephone_pere')->nullable();
            $table->string('email_pere')->nullable();

            $table->string('nom_mere')->nullable();
            $table->string('prenom_mere')->nullable();
            $table->string('telephone_mere')->nullable();
            $table->string('email_mere')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('enfants');
    }
}
