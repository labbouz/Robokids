<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHorairesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('horaires', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nom');
            $table->string('code');
            $table->integer('jour_en_semaine');
            $table->string('time_hiver_debut');
            $table->string('time_hiver_fin');
            $table->string('time_ete_debut');
            $table->string('time_ete_fin');
            $table->string('time_ramadan_debut');
            $table->string('time_ramadan_fin');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('horaires');
    }
}
