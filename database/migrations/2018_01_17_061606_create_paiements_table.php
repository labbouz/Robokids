<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaiementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('paiements', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('abonnement_id')->unsigned();
            $table->foreign('abonnement_id')->references('id')->on('abonnements');

            $table->string('mois_paiement');
            $table->string('anne_paiement');
            $table->decimal('montant', 5, 3)->default('0.000');
            $table->boolean('reglement');



            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('paiements');
    }
}
