<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAbonnementCalsseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('abonnement_calsse', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('abonnement_id')->unsigned();
            $table->foreign('abonnement_id')->references('id')->on('abonnements')->onDelete('cascade');

            $table->integer('calsse_id')->unsigned()->nullable();
            $table->foreign('calsse_id')->references('id')->on('calsses')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('abonnement_calsse');
    }
}
