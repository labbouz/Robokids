<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetailsPaiementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('details_paiements', function (Blueprint $table) {
            $table->increments('id');


            $table->integer('paiement_id')->unsigned();
            $table->foreign('paiement_id')->references('id')->on('paiements');

            $table->integer('calsse_id')->unsigned()->nullable();
            $table->foreign('calsse_id')->references('id')->on('calsses');
            $table->boolean('calsse_active')->default(true);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('details_paiements');
    }
}
