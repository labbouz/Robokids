<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//Route::resource('inscriptions', 'InscriptionController');
Route::get('/inscriptions/{id_formation}/create/', 'InscriptionController@create')->name('inscriptions.create');

//Route::get('/presences/{id_formation}', 'InscriptionController@create')->name('inscriptions.create');

Route::resource('abonnements', 'AbonnementController');
Route::get('/abonnements/list-inscrits/{id_formation}', 'InscriptionController@listInscrits')->name('abonnements.list');
Route::patch('/abonnements/desactiver_class/{abonnement_id}/{calsse_id}', 'AbonnementController@desactiver_class')->name('abonnements.desactiver_class');

Route::get('/paiements/{id_formation}', 'PaiementController@index')->name('paiements.list');
Route::post('/paiements/{id_formation}', 'PaiementController@index')->name('paiements.filter');
Route::post('/paiements-add', 'PaiementController@add')->name('paiements.add');
Route::patch('/paiements-update', 'PaiementController@update')->name('paiements.update');

Route::get('/abonnements/{id}/info', 'AbonnementController@show')->name('abonnements.info');
Route::get('/abonnements/{id}/paiements', 'AbonnementController@show')->name('abonnements.paiements');
Route::get('/abonnements/{id}/presences', 'AbonnementController@show')->name('abonnements.presences');
Route::get('/abonnements/{id}/enfant', 'AbonnementController@show')->name('abonnements.enfant');

Route::resource('modules', 'ModuleController');
Route::resource('niveaux', 'NiveauController');
Route::resource('groupes', 'GroupeController');
Route::resource('salles', 'SalleController');
Route::resource('horaires', 'HoraireController');
Route::resource('formations', 'FormationController');
Route::resource('enfants', 'EnfantController');

Route::get('/classes', 'CalsseController@sessionActive')->name('classes.all');
Route::get('/classes/display/{id_session}', 'CalsseController@index')->name('classes.index');
Route::get('/classes/determine/{id_session?}', 'CalsseController@determine')->name('classes.determine');
Route::get('/classes/determine/creat/{id_formation}/{id_horaire}/{id_salle}', 'CalsseController@creat')->name('classes.creat');
Route::post('/classes/determine', 'CalsseController@store')->name('classes.store');
Route::get('/classes/determine/edit/{id_classe}', 'CalsseController@edit')->name('classes.edit');
Route::patch('/classes/determine/{classe}', 'CalsseController@update')->name('classes.update');
Route::delete('/classes/determine/{classe}', 'CalsseController@destroy')->name('classes.destroy');

