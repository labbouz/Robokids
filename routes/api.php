<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
/*
Route::group(['middleware' => 'auth:api'], function() {
    Route::get('modules', 'ModuleController@index');
    Route::get('modules/{module}', 'ModuleController@show');
    Route::post('modules', 'ModuleController@store');
    Route::put('modules/{module}', 'ModuleController@update');
    Route::delete('modules/{module}', 'ModuleController@delete');
});
*/
