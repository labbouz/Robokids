<?php

// Home
Breadcrumbs::register('home', function ($breadcrumbs) {
    $breadcrumbs->push('Home', route('home'));
});

Breadcrumbs::register('abonnements.index', function ($breadcrumbs) {
    $breadcrumbs->push('Les abonnements', route('abonnements.index'));
});

/****
 * Les Paiements *
 * ********/

Breadcrumbs::register('paiements.list', function ($breadcrumbs, $formation) {
    $breadcrumbs->push('Les paiements pour la session '.$formation->nom, route('paiements.list', $formation->id));
});
/*******
 * ./Les Paiements *
 *******/


/****
 * Les Inscriptions *
 * ********/

Breadcrumbs::register('inscriptions.create', function ($breadcrumbs, $formation) {
    $breadcrumbs->push('Ajouter une inscription pour la session '.$formation->nom, route('inscriptions.create', $formation->id));
});

Breadcrumbs::register('abonnements.list', function ($breadcrumbs, $formation) {
    $breadcrumbs->push('Les abonnées pour la session '.$formation->nom, route('abonnements.list', $formation->id));
});

Breadcrumbs::register('abonnements.show', function ($breadcrumbs, $abonnement) {
    $breadcrumbs->parent('abonnements.list', $abonnement->formation);
    $breadcrumbs->push('Détail Abonnement ', route('abonnements.show', $abonnement->id));
});

Breadcrumbs::register('abonnements.paiements', function ($breadcrumbs, $abonnement) {
    $breadcrumbs->parent('paiements.list', $abonnement->formation);
    $breadcrumbs->push('Détail Paiement ', route('abonnements.show', $abonnement->id));
});

/*******
 * ./Les Inscriptions *
 *******/


/****
 * Les Enfants *
 * ********/

Breadcrumbs::register('enfants.index', function ($breadcrumbs) {
    $breadcrumbs->push('Les enfants', route('enfants.index'));
});

Breadcrumbs::register('enfants.create', function ($breadcrumbs) {
    $breadcrumbs->push('Ajouter un enfant', route('enfants.create'));
});

Breadcrumbs::register('enfants.edit', function ($breadcrumbs, $enfant) {
    $breadcrumbs->push('Modifier les détails qui concerne ' . $enfant->nom . ' ' . $enfant->prenom, route('enfants.edit', $enfant->id));
});

/*******
 * ./Les Enfants *
 *******/

/****
 * Les Classes *
 * ********/

Breadcrumbs::register('classes.index', function ($breadcrumbs, $formation) {
    $breadcrumbs->push('Tableau des horaires de la session '.$formation->nom, route('classes.index', $formation->id));
});

Breadcrumbs::register('classes.determine', function ($breadcrumbs, $formation) {
    $breadcrumbs->push('Répartition les classes de la session '.$formation->nom, route('classes.determine', $formation->id));
});

Breadcrumbs::register('classes.creat', function ($breadcrumbs, $formation) {
    $breadcrumbs->parent('classes.determine', $formation);
    $breadcrumbs->push('Ajouter une classe pour la session '.$formation->nom);
});

Breadcrumbs::register('classes.edit', function ($breadcrumbs, $classe) {
    $breadcrumbs->parent('classes.determine', $classe->formation);
    $breadcrumbs->push('Modifier la classe ' . $classe->groupe->nom . ' pour la session  ' . $classe->formation->nom, route('classes.edit', $classe->id));
});


/*******
 * ./Les Classes *
 *******/



/****
 * Les Formations *
 * ********/

Breadcrumbs::register('formations.index', function ($breadcrumbs) {
    $breadcrumbs->push('Les sessions de formation', route('formations.index'));
});

Breadcrumbs::register('formations.create', function ($breadcrumbs) {
    $breadcrumbs->parent('formations.index');
    $breadcrumbs->push('Ajouter une session de formation', route('formations.create'));
});

Breadcrumbs::register('formations.edit', function ($breadcrumbs, $formation) {
    $breadcrumbs->parent('formations.index');
    $breadcrumbs->push('Modifier la session de formation ' . $formation->nom, route('formations.edit', $formation->id));
});

/*******
 * ./Les Formations *
 *******/



/****
 * Les Horaires *
 * ********/

Breadcrumbs::register('horaires.index', function ($breadcrumbs) {
    $breadcrumbs->push('Les horaires', route('horaires.index'));
});

Breadcrumbs::register('horaires.create', function ($breadcrumbs) {
    $breadcrumbs->parent('horaires.index');
    $breadcrumbs->push('Ajouter un horaire', route('horaires.create'));
});

Breadcrumbs::register('horaires.edit', function ($breadcrumbs, $horaire) {
    $breadcrumbs->parent('horaires.index');
    $breadcrumbs->push('Modifier l\'horaire ' . $horaire->nom, route('horaires.edit', $horaire->id));
});

/*******
 * ./Les Horaires *
 *******/


/****
 * Les Salles *
 * ********/

Breadcrumbs::register('salles.index', function ($breadcrumbs) {
    $breadcrumbs->push('Les salles', route('salles.index'));
});

Breadcrumbs::register('salles.create', function ($breadcrumbs) {
    $breadcrumbs->parent('salles.index');
    $breadcrumbs->push('Ajouter une salle', route('salles.create'));
});

Breadcrumbs::register('salles.edit', function ($breadcrumbs, $salle) {
    $breadcrumbs->parent('salles.index');
    $breadcrumbs->push('Modifier la salle ' . $salle->nom, route('salles.edit', $salle->id));
});

/*******
 * ./Les Salles *
 *******/


/****
 * Les Groupes *
 * ********/

Breadcrumbs::register('groupes.index', function ($breadcrumbs) {
    $breadcrumbs->push('Les groupes', route('groupes.index'));
});

Breadcrumbs::register('groupes.create', function ($breadcrumbs) {
    $breadcrumbs->parent('groupes.index');
    $breadcrumbs->push('Ajouter un groupe', route('groupes.create'));
});

Breadcrumbs::register('groupes.edit', function ($breadcrumbs, $groupe) {
    $breadcrumbs->parent('groupes.index');
    $breadcrumbs->push('Modifier le groupe ' . $groupe->nom, route('groupes.edit', $groupe->id));
});

/*******
 * ./Les Groupes *
 *******/



/****
 * Les Niveaux *
 * ********/

Breadcrumbs::register('niveaux.index', function ($breadcrumbs) {
    $breadcrumbs->push('Les niveaux', route('niveaux.index'));
});

Breadcrumbs::register('niveaux.create', function ($breadcrumbs) {
    $breadcrumbs->parent('niveaux.index');
    $breadcrumbs->push('Ajouter un niveau', route('niveaux.create'));
});

Breadcrumbs::register('niveaux.edit', function ($breadcrumbs, $niveau) {
    $breadcrumbs->parent('niveaux.index');
    $breadcrumbs->push('Modifier le niveau ' . $niveau->nom, route('niveaux.edit', $niveau->id));
});

/*******
 * ./Les Niveaux *
 *******/


/****
 * Les Modules *
 * ********/

Breadcrumbs::register('modules.index', function ($breadcrumbs) {
    $breadcrumbs->push('Les modules', route('modules.index'));
});

Breadcrumbs::register('modules.create', function ($breadcrumbs) {
    $breadcrumbs->parent('modules.index');
    $breadcrumbs->push('Ajouter un module', route('modules.create'));
});

Breadcrumbs::register('modules.edit', function ($breadcrumbs, $module) {
    $breadcrumbs->parent('modules.index');
    $breadcrumbs->push('Modifier le module ' . $module->titre, route('modules.edit', $module->id));
});

/*******
 * ./Les Modules *
 *******/

/*
// Home > About


// Home > Blog
Breadcrumbs::register('blog', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Blog', route('blog'));
});

// Home > Blog > [Category]
Breadcrumbs::register('category', function ($breadcrumbs, $category) {
    $breadcrumbs->parent('blog');
    $breadcrumbs->push($category->title, route('category', $category->id));
});

// Home > Blog > [Category] > [Post]
Breadcrumbs::register('post', function ($breadcrumbs, $post) {
    $breadcrumbs->parent('category', $post->category);
    $breadcrumbs->push($post->title, route('post', $post));
});
*/