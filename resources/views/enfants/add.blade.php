@extends('layouts.app')

@section('title', 'Ajouter un enfant')

@section('style')
    <!-- Date picker plugins css -->
    <link href="{{ asset('backend/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}" rel="stylesheet">

@endsection

@section('content')


    <form class="form" method="POST" action="{{ route('enfants.store') }}" data-toggle="validator" enctype="multipart/form-data">
        {{ csrf_field() }}
    <!-- .row -->
    <div class="row">
        <div class="col-sm-12 col-md-12">
            <div class="white-box">
                <h3 class="box-title m-b-0">Formulaire d'ajout un nouveau enfant sur la base Robokids </h3>
                <p class="text-muted m-b-0 font-13"> Tous les champs en (*) sont obligatoires </p>

                @if ($errors->any())

                    @foreach ($errors->all() as $error)
                        <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            {{ $error }} </div>
                    @endforeach

                @endif
            </div>
        </div>


        <div class="col-md-12">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <div class="row">
                        <div class="col-sm-12 col-md-12">
                            <div class="white-box">
                                <div class="form-group row">
                                    <label for="nom" class="col-3 col-form-label">Nom (*)</label>
                                    <div class="col-9">
                                        <input class="form-control" type="text" value="" id="nom" name="nom" required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="prenom" class="col-3 col-form-label">Prénom (*)</label>
                                    <div class="col-9">
                                        <input class="form-control" type="text" value="" id="prenom" name="prenom" required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="date_naissance" class="col-3 col-form-label">Date de naissance (*)</label>
                                    <div class="col-9">
                                        <input class="form-control date-picker" type="text" value="" id="date_naissance" name="date_naissance" required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="genre" class="col-3 col-form-label">Genre (*)</label>
                                    <div class="col-9">

                                        <select class="form-control custom-select col-12" name="genre" id="genre" data-minlength="1" required>
                                            <option  value="" selected>Choisir...</option>
                                            @foreach($genres as $key_genre => $vlue_genre)
                                                <option value="{{ $key_genre }}">{{ $vlue_genre }}</option>
                                            @endforeach
                                        </select>

                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="ecole" class="col-3 col-form-label">&Eacute;cole (*)</label>
                                    <div class="col-9">
                                        <input class="form-control" type="text" value="" id="ecole" name="ecole" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-6">
                    <div class="row">
                        <div class="col-sm-12 col-md-12">
                            <div class="white-box">
                                <div class="form-group row">
                                    <label for="photo" class="col-3 col-form-label">Photo</label>
                                    <div class="col-9">
                                        <input class="form-control" type="file" value="" id="photo" name="photo">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-12 col-md-12">
                            <div class="white-box">
                                <div class="form-group row">
                                    <label for="adresse" class="col-3 col-form-label">Adresse (*)</label>
                                    <div class="col-9">
                                        <input class="form-control" type="text" value="" id="adresse" name="adresse" required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="ville" class="col-3 col-form-label">Ville</label>
                                    <div class="col-9">
                                        <input class="form-control" type="text" value="" id="ville" name="ville">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="code_postal" class="col-3 col-form-label">Code postle</label>
                                    <div class="col-9">
                                        <input class="form-control" type="number" value="" id="code_postal" name="code_postal">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <div class="col-md-6">
            <div class="white-box">
                <h3 class="box-title m-b-30">Informations Père </h3>
                <div class="form-group row">
                    <label for="nom_pere" class="col-3 col-form-label">Nom (*)</label>
                    <div class="col-9">
                        <input class="form-control" type="text" value="" id="nom_pere" name="nom_pere" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="prenom_pere" class="col-3 col-form-label">Prénom (*)</label>
                    <div class="col-9">
                        <input class="form-control" type="text" value="" id="prenom_pere" name="prenom_pere" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="telephone_pere" class="col-3 col-form-label">Mobile (*)</label>
                    <div class="col-9">
                        <input class="form-control" type="number" value="" id="telephone_pere" name="telephone_pere" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="email_pere" class="col-3 col-form-label">Email</label>
                    <div class="col-9">
                        <input class="form-control" type="email" value="" id="email_pere" name="email_pere">
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="white-box">
                <h3 class="box-title m-b-30">Informations Mère </h3>
                <div class="form-group row">
                    <label for="nom_mere" class="col-3 col-form-label">Nom (*)</label>
                    <div class="col-9">
                        <input class="form-control" type="text" value="" id="nom_mere" name="nom_mere" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="prenom_mere" class="col-3 col-form-label">Prénom (*)</label>
                    <div class="col-9">
                        <input class="form-control" type="text" value="" id="prenom_mere" name="prenom_mere" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="telephone_mere" class="col-3 col-form-label">Mobile (*)</label>
                    <div class="col-9">
                        <input class="form-control" type="number" value="" id="telephone_mere" name="telephone_mere" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="email_mere" class="col-3 col-form-label">Email</label>
                    <div class="col-9">
                        <input class="form-control" type="email" value="" id="email_mere" name="email_mere">
                    </div>
                </div>
            </div>
        </div>





            <div class="col-sm-12 col-md-12">
                <div class="white-box">
                    <button type="submit" class="btn btn-success waves-effect waves-light m-r-10">Enregisrer</button>
                    <a href="{{ route('enfants.index') }}" class="btn btn-inverse waves-effect waves-light">Annuler</a>
                </div>
            </div>

    </div>
    <!-- /.row -->
    </form>

@endsection

@section('scripts')
    <script src="{{ asset('backend/eliteadmin/js/custom.min.js') }}"></script>
    <script src="{{ asset('backend/eliteadmin/js/validator.js') }}"></script>


    <!-- Date Picker Plugin JavaScript -->
    <script src="{{ asset('backend/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('backend/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.fr.min.js') }}"></script>


    <script>
        $(document).ready(function() {


            jQuery('.date-picker').datepicker({
                toggleActive: true,
                language: "fr",
                format: "yyyy-mm-dd"
            });

        });
    </script>


@endsection