@extends('layouts.app')

@section('title', 'Les enfants')

@section('style')



@endsection

@section('content')

    <!-- .row -->
    <div class="row m-b-30">
        <div class="col-sm-12">
            <a href="{{ route('enfants.create') }}" class="btn btn-info btn-md btn-block">Ajouter un anfant</a>
        </div>

    </div>
    <!-- /.row -->

    @if(Session::has('success'))
        <div class="alert alert-success alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            {{ Session::get('success') }} </div>
    @endif

    <!-- .row -->
    <div class="row">
        <div class="col-sm-12">
            <div class="white-box">
                <div class="table-responsive">
                    <table class="table table-bordered table-hover color-bordered-table purple-bordered-table">
                        <thead>
                        <tr>
                            <th>ID Enfant</th>
                            <th>Nom</th>
                            <th>Prénom</th>
                            <th>Date de naissance</th>
                            <th>Genre</th>
                            <th>&Eacute;cole</th>
                            <th class="text-nowrap">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach( $enfants as $enfant)
                        <tr id="element_{{ $enfant->id }}">
                            <td><strong>{{ sprintf('%05d', $enfant->id) }}</strong></td>
                            <td>{{ $enfant->nom }}</td>
                            <td>{{ $enfant->prenom }}</td>
                            <td>{{ $enfant->date_naissance }}</td>
                            <td>{{ $enfant->genre }}</td>
                            <td>{{ $enfant->ecole }}</td>

                            <td class="text-nowrap">
                                <a href="{{ route('enfants.edit', $enfant->id) }}" data-toggle="tooltip" data-original-title="Modifier"> <i class="fa fa-pencil text-inverse m-r-10"></i> </a>
                                <a href="javascript:void(0);" data-url="{{ route('enfants.destroy', $enfant->id) }}" data-id="{{ $enfant->id }}" data-token="{{ csrf_token() }}" data-toggle="tooltip" data-original-title="Supprimer" class="sa-warning"> <i class="fa fa-close text-danger"></i> </a>
                            </td>
                        </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- /.row -->





@endsection

@section('scripts')
    <script src="{{ asset('backend/eliteadmin/js/custom.min.js') }}"></script>

    <script>
        $(document).ready(function() {

            //Warning Message
            $('.sa-warning').click(function(){
                var id = $(this).data("id");
                var token = $(this).data("token");
                var url = $(this).data("url");

                swal({
                    title: "Êtes-vous sûr?",
                    text: "Vous ne pourrez pas récupérer cet enfant!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Oui, supprimez-le!",
                    closeOnConfirm: false
                }, function(){

                    var _dataElemen = {
                        _token : token,
                        _method: "DELETE"
                    };


                    $.ajax({
                        type: 'DELETE',
                        url: url,
                        data: _dataElemen,
                        dataType: 'json',
                        success: function(data) {

                            if(data.status == 'success') {
                                $('#element_'+id).remove();

                                swal("Supprimé!", "L'enfant cible a été supprimé.", "success");
                            } else {
                                swal("Oops...", data.message, "error");
                            }


                        },
                        error: function(data){
                            //console.log(data);
                        }
                    });
                });
            });

        });
    </script>

@endsection