@extends('layouts.app')

@section('title', 'En total ( ' . $groupes->count() . ' ) Groupes' )

@section('style')
    <link href="{{ asset('backend/plugins/bower_components/datatables/jquery.dataTables.min.css') }}" rel="stylesheet">

@endsection

@section('content')

    @if(Session::has('success'))
        <div class="alert alert-success alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            {{ Session::get('success') }} </div>
    @endif

    <!-- .row -->
    <div class="row">
        <div class="col-sm-12">
            <div class="white-box">
                <div class="table-responsive">
                    <table id="table-groupes" class="table table-bordered table-hover color-bordered-table success-bordered-table">
                        <thead>
                        <tr>
                            <th>Le Groupe</th>
                            <th>Le Niveau</th>
                            <th>Le Module</th>
                            <th class="text-nowrap">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach( $groupes as $groupe)
                        <tr id="element_{{ $groupe->id }}">
                            <td>{{ $groupe->nom }}</td>
                            <td>{{ $groupe->niveau->nom }}</td>
                            <td>{{ $groupe->niveau->module->titre }}</td>
                            <td class="text-nowrap">
                                <a href="{{ route('groupes.edit', $groupe->id) }}" data-toggle="tooltip" data-original-title="Modifier"> <i class="fa fa-pencil text-inverse m-r-10"></i> </a>
                                <a href="javascript:void(0);" data-url="{{ route('groupes.destroy', $groupe->id) }}" data-id="{{ $groupe->id }}" data-token="{{ csrf_token() }}" data-toggle="tooltip" data-original-title="Supprimer" class="sa-warning"> <i class="fa fa-close text-danger"></i> </a>
                            </td>
                        </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- /.row -->

    <!-- .row -->
    <div class="row">
        <div class="col-sm-12">
            <a href="{{ route('groupes.create') }}" class="btn btn-info btn-md btn-block">Ajouter un groupe</a>
        </div>

    </div>
    <!-- /.row -->



@endsection

@section('scripts')
    <script src="{{ asset('backend/eliteadmin/js/custom.min.js') }}"></script>
    <script src="{{ asset('backend/plugins/bower_components/datatables/jquery.dataTables.min.js') }}"></script>

    <script>
        $(document).ready(function() {

            $('#table-groupes').DataTable({
                "paging":   false,
                "ordering": true,
                "info":     true,
                "language": {
                    "sProcessing":     "Traitement en cours...",
                    "sSearch":         "Rechercher&nbsp;:",
                    "sLengthMenu":     "Afficher _MENU_ &eacute;l&eacute;ments",
                    "sInfo":           "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                    "sInfoEmpty":      "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
                    "sInfoFiltered":   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                    "sInfoPostFix":    "",
                    "sLoadingRecords": "Chargement en cours...",
                    "sZeroRecords":    "Aucun &eacute;l&eacute;ment &agrave; afficher",
                    "sEmptyTable":     "Aucune donn&eacute;e disponible dans le tableau",
                    "oPaginate": {
                        "sFirst":      "Premier",
                        "sPrevious":   "Pr&eacute;c&eacute;dent",
                        "sNext":       "Suivant",
                        "sLast":       "Dernier"
                    },
                    "oAria": {
                        "sSortAscending":  ": activer pour trier la colonne par ordre croissant",
                        "sSortDescending": ": activer pour trier la colonne par ordre d&eacute;croissant"
                    }
                }
            });


            //Warning Message
            $('.sa-warning').click(function(){
                var id = $(this).data("id");
                var token = $(this).data("token");
                var url = $(this).data("url");

                swal({
                    title: "Êtes-vous sûr?",
                    text: "Vous ne pourrez pas récupérer ce groupe!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Oui, supprimez-le!",
                    closeOnConfirm: false
                }, function(){

                    var _dataElemen = {
                        _token : token,
                        _method: "DELETE"
                    };


                    $.ajax({
                        type: 'DELETE',
                        url: url,
                        data: _dataElemen,
                        dataType: 'json',
                        success: function(data) {

                            $('#element_'+id).remove();

                            swal("Supprimé!", "Votre niveau a été supprimé.", "success");
                        },
                        error: function(data){
                            //console.log(data);

                            swal("Supprimé!", "Votre niveau a été supprimé.", "success");
                        }
                    });
                });
            });

        });
    </script>

@endsection