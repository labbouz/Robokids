@extends('layouts.app')

@section('title', 'Détail Abonnement pour ' . $abonnement->enfant->prenom . ' ' . $abonnement->enfant->nom )

@section('style')

    <link href="{{ asset('backend/plugins/bower_components/switchery/dist/switchery.min.css') }}" rel="stylesheet">

    <style>
        .info-detail-paiement {
            display: none;
        }

        .fa-active-1 {
            color: #00a300;
            font-size: 18px !important;
        }

        .fa-active-1:before {
            content: "\f00c";
        }

        .fa-clock-o {
            color: #ffa200;
            font-size: 18px !important;
        }

        .fa-active-0 {
            color: #ff0000;
            font-size: 18px !important;
        }

        .fa-active-0:before {
            content: "\f00d";
        }

        .indicateur-status_paiement {
            display: none;
        }

        .indicateur-status_paiement .fa-active-1,
        .indicateur-status_paiement .fa-clock-o,
        .indicateur-status_paiement .fa-active-0 {
            font-size: 32px !important;
        }


        .icon-user-following {
            color: #00a300;
            font-size: 20px;
        }

        .icon-user-unfollow {
            color: #ff0000;
            font-size: 20px;
        }


    </style>


@endsection

@section('content')


    <!-- .row -->
    <div class="row">
        <div class="col-md-4 col-xs-12">
            <div class="white-box">
                <div class="user-bg">
                    <img width="100%" alt="user" src="{{ asset("backend/plugins/images/large/img1.jpg") }}">

                    <div class="overlay-box">
                        <div class="user-content">
                            <a href="javascript:void(0)">
                                @if($abonnement->enfant->photo)
                                    <img src="{{ asset($abonnement->enfant->photo) }}" class="thumb-lg img-circle" alt="img">
                                @else
                                    <img src="{{ asset("backend/plugins/images/users/genu.jpg") }}" class="thumb-lg img-circle" alt="img">
                                @endif
                            </a>
                            <h4 class="text-white">{{ $abonnement->enfant->prenom }}  {{ $abonnement->enfant->nom }} </h4>
                            <h3 class="text-white">{{ sprintf('%05d', $abonnement->enfant->id) }}</h3> </div>
                    </div>
                </div>
                <div class="user-btm-box">
                    @foreach( $abonnement->classes as $classe )
                    <div class="row">

                        <div class="col-md-12 col-sm-12 text-center">
                            <h2>{{ $classe->groupe->nom }}</h2>
                            <h3>{{ $classe->horaire->nom }}</h3>
                            <h4>{{ $classe->salle->nom }}</h4>
                        </div>
                    </div>
                        <hr>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="col-md-8 col-xs-12">
            <div class="white-box">
                <ul class="nav customtab nav-tabs" role="tablist">
                    <li role="presentation" class="nav-item"><a href="#home" class="nav-link {{ $class_onglet['enfant'] }}" aria-controls="home" role="tab" data-toggle="tab" aria-expanded="true"><span class="visible-xs"><i class="fa fa-home"></i></span><span class="hidden-xs">Enfant</span></a></li>
                    <li role="presentation" class="nav-item"><a href="#profile" class="nav-link {{ $class_onglet['info'] }}" aria-controls="profile" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="fa fa-user"></i></span> <span class="hidden-xs">Abonnement</span></a></li>
                    <li role="presentation" class="nav-item"><a href="#paiement" class="nav-link {{ $class_onglet['paiements'] }}" aria-controls="messages" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="fa fa-envelope-o"></i></span> <span class="hidden-xs">Paiement</span></a></li>
                    <li role="presentation" class="nav-item"><a href="#settings" class="nav-link {{ $class_onglet['presences'] }}" aria-controls="messages" role="tab" data-toggle="tab" aria-expanded="false"><span class="visible-xs"><i class="fa fa-cog"></i></span> <span class="hidden-xs">Présence</span></a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane {{ $class_onglet['enfant'] }}" id="home">
                        <h4 class="font-bold m-t-30">Information Enfant</h4>

                        <div class="row">
                            <div class="col-md-4 col-xs-6 b-r"> <strong>Nom</strong>
                                <br>
                                <p class="text-muted">{{ $abonnement->enfant->nom }}</p>
                            </div>
                            <div class="col-md-4 col-xs-6 b-r"> <strong>Prénom</strong>
                                <br>
                                <p class="text-muted">{{ $abonnement->enfant->prenom }}</p>
                            </div>

                            <div class="col-md-4 col-xs-6"> <strong>Genre</strong>
                                <br>
                                <p class="text-muted">{{ $genres[$abonnement->enfant->genre] }}</p>
                            </div>

                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-md-6 col-xs-6 b-r"> <strong>Date de naissance</strong>
                                <br>
                                <p class="text-muted">{{ \Carbon\Carbon::createFromFormat('Y-m-d', $abonnement->enfant->date_naissance)->formatLocalized('%d/%m/%Y') }}</p>
                            </div>
                            <div class="col-md-6 col-xs-6"> <strong>&Eacute;cole</strong>
                                <br>
                                <p class="text-muted">{{ $abonnement->enfant->ecole }}</p>
                            </div>

                        </div>
                        <h4 class="font-bold m-t-30">Information Adresse</h4>
                        <div class="row">
                            <div class="col-md-6 col-xs-6 b-r"> <strong>Adresse</strong>
                                <br>
                                <p class="text-muted">{{ $abonnement->enfant->adresse }}</p>
                            </div>
                            <div class="col-md-3 col-xs-6 b-r"> <strong>Ville</strong>
                                <br>
                                <p class="text-muted">{{ $abonnement->enfant->ville }}</p>
                            </div>

                            <div class="col-md-3 col-xs-6"> <strong>Code postle</strong>
                                <br>
                                <p class="text-muted">{{ $abonnement->enfant->code_postal }}</p>
                            </div>

                        </div>

                        <h4 class="font-bold m-t-30">Informations Père</h4>
                        <div class="row">
                            <div class="col-md-3 col-xs-6 b-r"> <strong>Prénom</strong>
                                <br>
                                <p class="text-muted">{{ $abonnement->enfant->prenom_pere }}</p>
                            </div>
                            <div class="col-md-3 col-xs-6 b-r"> <strong>Nom</strong>
                                <br>
                                <p class="text-muted">{{ $abonnement->enfant->nom_pere }}</p>
                            </div>
                            <div class="col-md-3 col-xs-6 b-r"> <strong>Mobile</strong>
                                <br>
                                <p class="text-muted">{{ $abonnement->enfant->telephone_pere }}</p>
                            </div>

                            <div class="col-md-3 col-xs-6"> <strong>Email</strong>
                                <br>
                                <p class="text-muted">{{ $abonnement->enfant->email_pere }}</p>
                            </div>

                        </div>

                        <h4 class="font-bold m-t-30">Informations Mère</h4>
                        <div class="row">
                            <div class="col-md-3 col-xs-6 b-r"> <strong>Prénom</strong>
                                <br>
                                <p class="text-muted">{{ $abonnement->enfant->prenom_mere }}</p>
                            </div>
                            <div class="col-md-3 col-xs-6 b-r"> <strong>Nom</strong>
                                <br>
                                <p class="text-muted">{{ $abonnement->enfant->nom_mere }}</p>
                            </div>
                            <div class="col-md-3 col-xs-6 b-r"> <strong>Mobile</strong>
                                <br>
                                <p class="text-muted">{{ $abonnement->enfant->telephone_mere }}</p>
                            </div>

                            <div class="col-md-3 col-xs-6"> <strong>Email</strong>
                                <br>
                                <p class="text-muted">{{ $abonnement->enfant->email_mere }}</p>
                            </div>

                        </div>

                    </div>
                    <div class="tab-pane {{ $class_onglet['info'] }}" id="profile">

                        <h3 class="font-bold m-b-30">Information Abonnement</h3>

                        <div class="row">

                            <div class="col-md-4 col-xs-6 b-r text-center">
                                <p class="font-bold m-t-0 m-b-30"><strong>Date Début</strong></p>
                                <p class="text-muted"><strong>{{ \Carbon\Carbon::createFromFormat('Y-m-d', $abonnement->date_debut_abonnement)->formatLocalized('%d/%m/%Y') }}</strong></p>
                            </div>
                            <div class="col-md-4 col-xs-6 b-r text-center">
                                <p class="font-bold m-t-0 m-b-30"><strong>Date Fin</strong></p>
                                <p class="text-muted"><strong>{{ \Carbon\Carbon::createFromFormat('Y-m-d', $abonnement->date_fin_abonnement)->formatLocalized('%d/%m/%Y') }}</strong></p>
                            </div>
                            <div class="col-md-4 col-xs-6 text-center">
                                <p class="font-bold m-t-0 m-b-30"><strong>Statut</strong></p>
                                <p class="text-muted"><strong>{{ $abonnement->status }}</strong></p>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-md-12 col-xs-12">
                                <h3 class="font-bold m-t-30">Nombre de classes : {{ $abonnement->classes->count() }}</h3>
                            </div>
                        </div>
                        @foreach( $abonnement->classes as $classe)
                            
							<hr>
                            <div class="row">

                                <div class="col-md-3 col-xs-6 b-r text-center">
                                    <h3 class="font-bold m-t-0 m-b-30">{{ $classe->groupe->nom }}</h3>
                                    <p class="text-muted"><strong>{{ $classe->groupe->niveau->module->titre }}</strong></p>
                                    <p class="text-muted"><strong>{{ $classe->groupe->niveau->nom }}</strong></p>
                                    <p class="text-muted">
                                        <input type="checkbox" class="js-switch abonnement_active" data-color="#99d683" data-secondary-color="#f96262" value="1"
                                               data-url="{{ route('abonnements.desactiver_class', ['abonnement_id' => $abonnement->id, 'calsse_id' => $classe->id ] ) }}" data-token="{{ csrf_token() }}"
                                               @if($classe->pivot->active)
                                                checked
                                                @endif
                                        />
                                    </p>
                                </div>
                                <div class="col-md-6 col-xs-6 b-r"> <strong>Horaires</strong><br><br>
                                        <p class="text-muted">{{ $classe->horaire->nom }}  / <strong>{{ $classe->horaire->code }}</strong></p>
                                        <p class="text-muted">Le jour : <strong>{{ $jours[$classe->horaire->jour_en_semaine] }}</strong></p>
                                        <p class="text-muted">Horaires d'hiver : <strong>{{ $classe->horaire->time_hiver_debut }}</strong> --> <strong>{{ $classe->horaire->time_hiver_fin }}</strong></p>
                                        <p class="text-muted">Horaires d'été : <strong>{{ $classe->horaire->time_ete_debut }}</strong> --> <strong>{{ $classe->horaire->time_ete_fin }}</strong></p>
                                        <p class="text-muted">Horaires de Ramadhan : <strong>{{ $classe->horaire->time_ramadan_debut }}</strong> --> <strong>{{ $classe->horaire->time_ramadan_fin }}</strong></p>
                                </div>
                                <div class="col-md-3 col-xs-6"> <strong>Salle</strong><br><br>
                                        <p class="text-muted">{{ $classe->salle->nom }} / <strong>{{ $classe->salle->code }}</strong></p>
                                </div>
                            </div>
							
                        @endforeach

                        </div>
                    <div class="tab-pane {{ $class_onglet['paiements'] }}" id="paiement">

                        <div class="row">
                            <div class="col-sm-12">

                                <div class="table-responsive">
                                    <table id="table-groupes" class="table table-bordered display">
                                        <thead>
                                        <tr>
                                            <th>Classe</th>
                                            @foreach($dates_months as $date_month)
                                                @php
                                                    $class_current_month_active = '';
                                                    if($date_month['mois_courant']) {
                                                        $class_current_month_active = 'active-col';
                                                    }
                                                @endphp

                                                <th class="text-center {{ $class_current_month_active }}">
                                                    <big>
                                                        {{ $date_month['mois'] }}
                                                    </big>
                                                    <br>
                                                    <small>
                                                        {{ $date_month['anne'] }}
                                                    </small>
                                                </th>
                                            @endforeach

                                        </tr>
                                        </thead>
                                        <tbody>
                                        @php

                                        @endphp
                                        @foreach( $abonnement->classes as $classe)
                                            <tr id="element_{{ $classe->id }}">
                                                <td>{{ $classe->groupe->nom }}</td>
                                                @foreach($dates_months as $date_month)
                                                    @php
                                                        $class_current_month_active = '';
                                                        if($date_month['mois_courant']) {
                                                            $class_current_month_active = 'active-col';
                                                        }

                                                        $date_fin_participation = array();
                                                        $date_fin_participation['anne'] = $classe->pivot->updated_at->format('Y');
                                                        $date_fin_participation['mois'] = $classe->pivot->updated_at->format('n');




                                                    @endphp
                                                    <td class="text-center {{ $class_current_month_active }}">
                                                        @if($classe->pivot->active)
                                                            <i class="icon-user-following"></i>
                                                        @else
                                                            @if( $date_month['anne'] < $date_fin_participation['anne'])
                                                                <i class="icon-user-following"></i>
                                                            @else
                                                                @if( $date_month['anne'] == $date_fin_participation['anne'] &&  $date_month['mois'] < $date_fin_participation['mois'] )
                                                                    <i class="icon-user-following"></i>
                                                                @else
                                                                    <i class="icon-user-unfollow"></i>
                                                                @endif

                                                            @endif


                                                        @endif

                                                    </td>
                                                @endforeach
                                            </tr>
                                        @endforeach


                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>Réglement</th>
                                                @foreach($dates_months as $date_month)
                                                    @php
                                                        $class_current_month_active = '';
                                                        if($date_month['mois_courant']) {
                                                            $class_current_month_active = 'active-col';
                                                        }
                                                    @endphp
                                                    <th class="text-center {{ $class_current_month_active }}">
                                                        <a
                                                                href="javascript:void(0);"
                                                                class="info_paiement"
                                                                data-anne="{{ $date_month['anne'] }}"
                                                                data-mois="{{ $date_month['mois'] }}"
                                                                data-reglement-paiement="{{ $datas_paiements[$date_month['anne'].'_'.$date_month['mois']]['reglement'] }}"
                                                                data-montant-paiement="{{ $datas_paiements[$date_month['anne'].'_'.$date_month['mois']]['montant'] }}"
                                                                data-id-paiement="{{ $datas_paiements[$date_month['anne'].'_'.$date_month['mois']]['id'] }}"
                                                                data-status-paiement="{{ $datas_paiements[$date_month['anne'].'_'.$date_month['mois']]['status_paiement'] }}"
                                                                data-remise-paiement="{{ intval($datas_paiements[$date_month['anne'].'_'.$date_month['mois']]['remise']) }}"

                                                                @foreach($data_classes_info as $data_classe)

                                                                    @if($datas_paiements[$date_month['anne'].'_'.$date_month['mois']]['remise'])
                                                                        data-ligne-class-{{ $data_classe['id'] }}="1"
                                                                    @else
                                                                        data-ligne-class-{{ $data_classe['id'] }}="{{ $data_classe['active'] }}"
                                                                    @endif

                                                                @endforeach


                                                        >
                                                            @if($datas_paiements[$date_month['anne'].'_'.$date_month['mois']]['status_paiement'] == 1)
                                                                <i class="fa fa-active-1"></i></a>
                                                            @elseif($datas_paiements[$date_month['anne'].'_'.$date_month['mois']]['status_paiement'] == 0)
                                                                <i class="fa fa-clock-o"></i></a>
                                                            @else
                                                                <i class="fa fa-active-0"></i></a>
                                                            @endif



                                                    </th>
                                                @endforeach

                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>

                            </div>
                        </div>

                        <div class="row info-detail-paiement" id="tableau_paiement">
                            <div class="col-sm-12">
                                <div class="table-responsive">

                                    <table id="listes_classes_selectionnees" class="table table-bordered color-table inverse-table">
                                        <thead>

                                        <tr id="ligne_info_date">
                                            <th>
                                                Mois du paiement
                                            </th>
                                            <th class="text-center">
                                                <strong id="label_mois"></strong> <strong>.</strong> <strong id="label_anne"></strong>


                                            </th>
                                        </tr>
                                        <tr>
                                            <th>Objet de paiement</th>
                                            <th class="text-center">Le frais en TND</th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        @foreach($abonnement->classes as $classe)
                                            
											@if($classe->pivot->active)
							
											<tr id="ligne_classe_{{ $classe->id }}" class="line-class-display" data-groupe-class="{{ $classe->id }}">
                                                <td>
                                                    Frais session pour le module <strong id="label_groupe_{{ $classe->id }}">{{ $classe->groupe->nom }}</strong>
                                                </td>
                                                <td class="text-center"><strong>{{ $formation->frais_session }}</strong></td>
                                            </tr>
											
											@endif
											
                                        @endforeach

                                        <tr id="ligne_classe_remise_2">
                                            <td class="text-right">
                                                Frais mensuelle en totale aprés remise 2 classes ( <strong>{{ $formation->pourcentage_double }} %</strong> ) :
                                            </td>
                                            @php
                                                $caclule_prix = ( $formation->frais_session * 2 ) - ($formation->frais_session * 2 * $formation->pourcentage_double / 100 )
                                            @endphp
                                            <td class="text-center"><strong id="label_total">{{ number_format($caclule_prix,3) }}</strong></td>
                                        </tr>
										
										<tr id="ligne_classe_remise_3">
                                            <td class="text-right">
                                                Frais mensuelle en totale aprés remise 3 classes ( <strong>{{ $formation->pourcentage_triple }} %</strong> ) :
                                            </td>
                                            @php
                                                $caclule_prix = ( $formation->frais_session * 3 ) - ($formation->frais_session * 3 * $formation->pourcentage_triple / 100 )
                                            @endphp
                                            <td class="text-center"><strong id="label_total">{{ number_format($caclule_prix,3) }}</strong></td>
                                        </tr>

                                        <tr id="ligne_classe_sans_remise">
                                            <td class="text-right">
                                                Frais mensuelle en totale payé :
                                            </td>
                                            <td class="text-center"><strong id="label_total_paye">{{ $formation->frais_session }}</strong></td>
                                        </tr>


                                        <tr>
                                            <td class="text-right">

                                                <input type="hidden" name="formation_id" value="{{ $formation->id }}">

                                                <div class="row">
                                                    <label for="active" class="col-12 col-form-label text-right">Reglement ?</label>
                                                    <div class="col-12">
                                                        <div class="btn-reglement">
                                                            <input name="reglement" id="reglement" type="checkbox" class="reglement_paiement" value="1" checked />
                                                            <input id="reglement_old" type="hidden" value="" />
                                                        </div>

                                                    </div>
                                                </div>

                                            </td>
                                            <td class="text-center">
                                                <span class="indicateur-status_paiement status_paiement-active"><i class="fa fa-active-1"></i></span>
                                                <span class="indicateur-status_paiement status_paiement-pandding"><i class="fa fa-clock-o"></i></span>
                                                <span class="indicateur-status_paiement status_paiement-non"><i class="fa fa fa-active-0"></i></span>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td colspan="2">
                                                <input id="url-add-paiment" type="hidden" data-url="{{ route('paiements.add') }}" data-token="{{ csrf_token() }}" >
                                                <input id="url-update-paiment" type="hidden" data-url="{{ route('paiements.update') }}" data-token="{{ csrf_token() }}" >
                                                <input type="hidden" name="paiement_id" id="paiement_id" value="{{ $abonnement->id }}">
                                                <input type="hidden" name="abonnement_id" id="abonnement_id" value="{{ $abonnement->id }}">
                                                <input type="hidden" name="mois_paiement" id="mois_paiement" value="">
                                                <input type="hidden" name="anne_paiement" id="anne_paiement" value="">
                                                <button type="button" class="btn btn-info btn-md btn-block change-status-paiement">ENREGISTRE</button>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>


                    </div>
                    <div class="tab-pane {{ $class_onglet['presences'] }}" id="settings">

                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->


    </div>



    @endsection

    @section('scripts')
        <script src="{{ asset('backend/eliteadmin/js/custom.min.js') }}"></script>
        <script src="{{ asset('backend/eliteadmin/js/validator.js') }}"></script>

        <script src="{{ asset('backend/plugins/bower_components/switchery/dist/switchery.min.js') }}"></script>

        <script>
            $(document).ready(function() {

                // Switchery
                var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
                $('.js-switch').each(function() {
                    new Switchery($(this)[0], $(this).data());
                });



                $('.abonnement_active').change(function() {

                    var action_activation = 0;
                    var retourChecked = 1;

                    if(this.checked) {
                        action_activation = 1;
                        retourChecked = 0;
                    }

                    var seelctor_checked = $(this);

                    var token = $(this).data("token");
                    var url = $(this).data("url");

                    swal({
                        title: "Êtes-vous sûr?",
                        text: "",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Oui, changez l'état!",
                        closeOnConfirm: false
                    }, function(isConfirm){

                        if (isConfirm) {
                            var _dataElemen = {
                                _type_action : action_activation,
                                _token : token,
                                _method: "PATCH"
                            };


                            $.ajax({
                                type: 'POST',
                                url: url,
                                data: _dataElemen,
                                dataType: 'json',
                                success: function(data) {

                                    if(data.status == 'success') {

                                        // swal("Succes!", "L'abonnement de cette class a été changé.", "success");

                                        swal({
                                            title: "Succes!",
                                            text: "L'abonnement de cette class a été changé",
                                            type: "success"
                                        }, function() {
                                            location.reload();
                                        });


                                    } else {
                                        swal("Oops...", data.message, "error");
                                    }


                                },
                                error: function(data){
                                    //console.log(data);
                                }
                            });
                        } else {
                            seelctor_checked.trigger('click');


                        }


                    });

                });



                var reglementSwitch = new Switchery($('.reglement_paiement')[0], {
                    color: '#99d683',
                    secondaryColor    : '#f96262'
                });


                $( ".info_paiement" ).click(function() {

                    $('.indicateur-status_paiement').hide();

                    var selectoPaiement = $(this);

                    var _anne = $(this).attr('data-anne');
                    var _mois = $(this).attr('data-mois');
                    var _montant_paiement = $(this).attr('data-montant-paiement');
                    var _reglement_paiement = $(this).attr('data-reglement-paiement');
                    var _remise_paiement = $(this).attr('data-remise-paiement');
                    var _status_paiement = $(this).attr('data-status-paiement');
                    var _id_paiement = $(this).attr('data-id-paiement');

                    $('#mois_paiement').val(_mois);
                    $('#anne_paiement').val(_anne);
                    $('#reglement_old').val(_reglement_paiement);
                    $('#paiement_id').val(_id_paiement);

                    $('#label_mois').text(_mois);
                    $('#label_anne').text(_anne);
                    $('#label_total_paye').text(_montant_paiement);

                    if(_reglement_paiement == '0') {
                        //Checks the switch
                        setSwitchery(reglementSwitch, false);
                    } else {
                        //Unchecks the switch
                        setSwitchery(reglementSwitch, true);
                    }

                    if(_remise_paiement == '2' ) {
                        $('#ligne_classe_remise_2').show();
						$('#ligne_classe_remise_3').hide();
                    } else {
						if(_remise_paiement == '3' ) {
							$('#ligne_classe_remise_3').show();
							$('#ligne_classe_remise_2').hide();
						} else {
							$('#ligne_classe_remise_2').hide();
							$('#ligne_classe_remise_3').hide();
						}
					}

                    if(_status_paiement == "1") {
                        $('.status_paiement-active' ).show();
                    } else {
                        if(_status_paiement == "0") {
                            $('.status_paiement-pandding' ).show();
                        } else {
                            $('.status_paiement-non' ).show();
                        }
                    }


                    $('.line-class-display').each(function(){
                        var _ligneSelector = $(this);
                        var _idclass = _ligneSelector.attr('data-groupe-class');

                        var _activeClass = selectoPaiement.attr('data-ligne-class-'+_idclass);


                        if(_activeClass == '1'){
                            _ligneSelector.show();
                        } else {
                            _ligneSelector.hide();
                        }


                    });







                    $('.info-detail-paiement').show(500);
                });

                function setSwitchery(switchElement, checkedBool) {
                    if((checkedBool && !switchElement.isChecked()) || (!checkedBool && switchElement.isChecked())) {
                        switchElement.setPosition(true);
                        switchElement.handleOnchange(true);
                    }
                }


                $('.change-status-paiement').click(function() {

                    var _id = $('#paiement_id').val();
                    var _mois = $('#mois_paiement').val();
                    var _anne = $('#anne_paiement').val();
                    var _abonnement_id = $('#abonnement_id').val();
                    var _reglement;

                    if($("#reglement").is(':checked')) {
                        _reglement = 1;  // checked
                    } else {
                        _reglement = 0;
                    }


                    var oldReglement = $('#reglement_old').val();


                    swal({
                        title: "Êtes-vous sûr?",
                        text: "",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Oui, changez l'état!",
                        closeOnConfirm: false
                    }, function(isConfirm){

                        if (isConfirm) {



                            var seelctor_checked;
                            if(_id == '0') {
                                seelctor_checked = $("#url-add-paiment");

                                var token = seelctor_checked.data("token");
                                var url = seelctor_checked.data("url");



                                var _dataElemen = {
                                    id : _id,
                                    abonnement_id : _abonnement_id,
                                    mois_paiement : _mois,
                                    anne_paiement : _anne,
                                    reglement : _reglement,
                                    _token : token,
                                    _method: "POST"
                                };

                            } else {
                                seelctor_checked = $("#url-update-paiment");

                                var token = seelctor_checked.data("token");
                                var url = seelctor_checked.data("url");

                                var _dataElemen = {
                                    id : _id,
                                    reglement : _reglement,
                                    _token : token,
                                    _method: "PATCH"
                                };


                            }

                            $('.indicateur-status_paiement').hide();
                            $('.btn-reglement').hide();


                            $.ajax({
                                type: 'POST',
                                url: url,
                                data: _dataElemen,
                                dataType: 'json',
                                success: function(data) {


                                    $('.btn-reglement').show();

                                    if(_reglement == "1") {
                                        $('.status_paiement-active').show();
                                    } else {
                                        $('.status_paiement-non').show();
                                    }
                                    $('#reglement_old').val(_reglement);

                                    if(data.status == 'success') {

                                        // swal("Succes!", "L'abonnement de cette class a été changé.", "success");

                                        swal({
                                            title: "Succes!",
                                            text: "Le mode de paiment a été changé",
                                            type: "success"
                                        });

                                        $('.info_paiement').each(function(){
                                            var selectorLink = $(this);
                                            var anne = selectorLink.data("anne");
                                            var mois = selectorLink.data("mois");



                                            if(anne == _anne && mois == _mois  ) {


                                                selectorLink.attr('data-id-paiement', data.id);

                                                selectorLink.attr('data-reglement-paiement', _reglement);

                                                if(_reglement == '1') {

                                                    selectorLink.attr('data-status-paiement', '1');
                                                    selectorLink.find('i').attr('class', 'fa fa-active-1');
                                                } else {
                                                    selectorLink.attr('data-status-paiement', '-1');
                                                    selectorLink.find('i').attr('class', 'fa fa-active-0');
                                                }





                                            }
                                        });


                                    } else {
                                        swal("Oops...", data.message, "error");
                                    }


                                },
                                error: function(data){
                                    //console.log(data);
                                }
                            });

                        } else {

                            if(oldReglement == '0') {
                                //Checks the switch
                                setSwitchery(reglementSwitch, false);
                            } else {
                                //Unchecks the switch
                                setSwitchery(reglementSwitch, true);
                            }


                        }


                    });

                });





            });
        </script>

    @endsection