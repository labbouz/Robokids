@if($formation_active)
    
<li><a href="{{ route('classes.all') }}" class="waves-effect"><i data-icon="&#xe001;" class="linea-icon linea-basic fa-fw"></i> <span class="hide-menu">Session {{ $formation_active->nom }} <span class="fa arrow"></span></span></a>
    <ul class="nav nav-second-level">
	{{--<li> <a href="{{ route('abonnements.index') }}">Présences</a> </li>--}}
        <li> <a href="{{ route('paiements.list', $formation_active->id) }}">Paiements</a> </li>
        <li> <a href="{{ route('abonnements.list', $formation_active->id) }}">Les abonnées</a> </li>
        <li> <a href="{{ route('inscriptions.create', $formation_active->id) }}">Inscription</a> </li>
        <li> <a href="{{ route('classes.index', $formation_active->id) }}">Tableau des horaires</a> </li>
        <li> <a href="{{ route('classes.determine', $formation_active->id) }}">Répartition les classes</a> </li>
    </ul>
</li>

@endif
