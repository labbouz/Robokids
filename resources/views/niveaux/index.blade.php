@extends('layouts.app')

@section('title', 'Les niveaux')

@section('style')



@endsection

@section('content')

    @if(Session::has('success'))
        <div class="alert alert-success alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            {{ Session::get('success') }} </div>
    @endif

    <!-- .row -->
    <div class="row">
        <div class="col-sm-12">
            <div class="white-box">
                <div class="table-responsive">
                    <table class="table table-bordered table-hover color-bordered-table purple-bordered-table">
                        <thead>
                        <tr>
                            <th>Le Module</th>
                            <th>Le Niveau</th>
                            <th>Rang</th>

                            <th class="text-nowrap">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach( $niveaux as $niveau)
                        <tr id="element_{{ $niveau->id }}">
                            <td>{{ $niveau->module->titre }}</td>
                            <td>{{ $niveau->nom }}</td>
                            <td><span class="label label-info">{{ $niveau->rang }}</span></td>
                            <td class="text-nowrap">
                                <a href="{{ route('niveaux.edit', $niveau->id) }}" data-toggle="tooltip" data-original-title="Modifier"> <i class="fa fa-pencil text-inverse m-r-10"></i> </a>
                                <a href="javascript:void(0);" data-url="{{ route('niveaux.destroy', $niveau->id) }}" data-id="{{ $niveau->id }}" data-token="{{ csrf_token() }}" data-toggle="tooltip" data-original-title="Supprimer" class="sa-warning"> <i class="fa fa-close text-danger"></i> </a>
                            </td>
                        </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- /.row -->

    <!-- .row -->
    <div class="row">
        <div class="col-sm-12">
            <a href="{{ route('niveaux.create') }}" class="btn btn-info btn-md btn-block">Ajouter un niveau</a>
        </div>

    </div>
    <!-- /.row -->



@endsection

@section('scripts')
    <script src="{{ asset('backend/eliteadmin/js/custom.min.js') }}"></script>

    <script>
        $(document).ready(function() {

            //Warning Message
            $('.sa-warning').click(function(){
                var id = $(this).data("id");
                var token = $(this).data("token");
                var url = $(this).data("url");

                swal({
                    title: "Êtes-vous sûr?",
                    text: "Vous ne pourrez pas récupérer ce niveau!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Oui, supprimez-le!",
                    closeOnConfirm: false
                }, function(){

                    var _dataElemen = {
                        _token : token,
                        _method: "DELETE"
                    };


                    $.ajax({
                        type: 'DELETE',
                        url: url,
                        data: _dataElemen,
                        dataType: 'json',
                        success: function(data) {


                            if(data.status == 'success') {
                                $('#element_'+id).remove();

                                swal("Supprimé!", "Votre niveau a été supprimé.", "success");
                            } else {
                                swal("Oops...", data.message, "error");
                            }

                        },
                        error: function(data){
                            //console.log(data);
                        }
                    });
                });
            });

        });
    </script>

@endsection