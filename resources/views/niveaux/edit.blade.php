@extends('layouts.app')

@section('title', 'Modifier le niveau ' . $niveau->nom )

@section('style')



@endsection

@section('content')




    <!-- .row -->
    <div class="row">
        <div class="col-sm-12">
            <div class="white-box">
                <h3 class="box-title m-b-0">Formulaire de modification d'un niveau</h3>
                <p class="text-muted m-b-30 font-13"> Tous les champs en (*) sont obligatoires </p>

                @if ($errors->any())

                    @foreach ($errors->all() as $error)
                        <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            {{ $error }} </div>
                    @endforeach

                @endif

                <form class="form" method="POST" action="{{ route('niveaux.update', $niveau->id) }}" data-toggle="validator">
                    {{ csrf_field() }}
                    {{ method_field('PATCH') }}

                    <div class="form-group row">
                        <label for="module_id" class="col-2 col-form-label">Module (*)</label>
                        <div class="col-10">
                            <select class="form-control custom-select col-12" name="module_id" id="module_id" data-minlength="1" required>
                                <option  value="">Choisir...</option>
                                @foreach($modules as $module)
                                    <option value="{{ $module->id }}"
                                    @if($module->id == $niveau->module_id)
                                    selected
                                    @endif
                                    >{{ $module->titre }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="titre" class="col-2 col-form-label">Nom (*)</label>
                        <div class="col-10">
                            <input class="form-control" type="text" value="{{ $niveau->nom }}" id="nom" name="nom" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="code" class="col-2 col-form-label">Code (*)</label>
                        <div class="col-10">
                            <input class="form-control" type="number" value="{{ $niveau->rang }}" id="rang" name="rang" min="1" required>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-success waves-effect waves-light m-r-10">Enregisrer</button>
                    <a href="{{ route('niveaux.index') }}" class="btn btn-inverse waves-effect waves-light">Annuler</a>
                </form>
            </div>
        </div>
    </div>
    <!-- /.row -->

@endsection

@section('scripts')
    <script src="{{ asset('backend/eliteadmin/js/custom.min.js') }}"></script>
    <script src="{{ asset('backend/eliteadmin/js/validator.js') }}"></script>

    <script>
        $(document).ready(function() {


        });
    </script>

@endsection