@extends('layouts.app')

@section('title', 'Ajouter un horaire')

@section('style')

    <link href="{{ asset('backend/plugins/bower_components/clockpicker/dist/jquery-clockpicker.min.css') }}" rel="stylesheet">

@endsection

@section('content')




    <!-- .row -->
    <div class="row">
        <div class="col-sm-12">
            <div class="white-box">
                <h3 class="box-title m-b-0">Formulaire de création un nouveau horaire</h3>
                <p class="text-muted m-b-30 font-13"> Tous les champs en (*) sont obligatoires </p>

                @if ($errors->any())

                    @foreach ($errors->all() as $error)
                        <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            {{ $error }} </div>
                    @endforeach

                @endif

                <form class="form" method="POST" action="{{ route('horaires.store') }}" data-toggle="validator">
                    {{ csrf_field() }}

                    <div class="form-group row">
                        <label for="nom" class="col-2 col-form-label">Nom (*)</label>
                        <div class="col-10">
                            <input class="form-control" type="text" value="" id="nom" name="nom" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="code" class="col-2 col-form-label">Code (*)</label>
                        <div class="col-10">
                            <input class="form-control" type="text" value="" id="code" name="code" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="jour_en_semaine" class="col-2 col-form-label">Le jour (*)</label>
                        <div class="col-10">
                            <select class="form-control custom-select col-12" name="jour_en_semaine" id="jour_en_semaine" data-minlength="1" required>
                                <option  value="" selected>Choisir...</option>
                                @foreach($jours as $key_j => $jour)
                                    <option value="{{ $key_j }}">{{ $jour }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>



                    <div class="row">
                        <div class="col-4">

                            <div class="form-group row">
                                <strong>Horaires d'hiver</strong>
                            </div>

                            <div class="form-group row">
                                <label for="time_hiver_debut" class="col-12 col-form-label">Heure Début (*)</label>
                                <div class="col-12">
                                    <div class="input-group clockpicker">
                                        <input type="text" name="time_hiver_debut" id="time_hiver_debut" class="form-control" pattern="[0-2]{1}[0-9]{1}:[0-5]{1}[0-9]{1}" value="08:30" required> <span class="input-group-addon"> <span class="glyphicon glyphicon-time"></span> </span>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="time_hiver_fin" class="col-12 col-form-label">Heure Fin (*)</label>
                                <div class="col-12">
                                    <div class="input-group clockpicker">
                                        <input type="text" name="time_hiver_fin" id="time_hiver_fin" class="form-control" pattern="[0-2]{1}[0-9]{1}:[0-5]{1}[0-9]{1}" value="10:30" required> <span class="input-group-addon"> <span class="glyphicon glyphicon-time"></span> </span>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="col-4">
                            <div class="form-group row">
                                <strong>Horaires d'été</strong>
                            </div>

                            <div class="form-group row">
                                <label for="time_ete_debut" class="col-12 col-form-label">Heure Début (*)</label>
                                <div class="col-12">
                                    <div class="input-group clockpicker">
                                        <input type="text" name="time_ete_debut" id="time_ete_debut" class="form-control" pattern="[0-2]{1}[0-9]{1}:[0-5]{1}[0-9]{1}" value="09:00" required> <span class="input-group-addon"> <span class="glyphicon glyphicon-time"></span> </span>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="time_ete_fin" class="col-12 col-form-label">Heure Fin (*)</label>
                                <div class="col-12">
                                    <div class="input-group clockpicker">
                                        <input type="text"  name="time_ete_fin" id="time_ete_fin" class="form-control" pattern="[0-2]{1}[0-9]{1}:[0-5]{1}[0-9]{1}" value="11:00" required> <span class="input-group-addon"> <span class="glyphicon glyphicon-time"></span> </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="form-group row">
                                <strong>Horaires de Ramadhan</strong>
                            </div>

                            <div class="form-group row">
                                <label for="time_ramadan_debut" class="col-12 col-form-label">Heure Début (*)</label>
                                <div class="col-12">
                                    <div class="input-group clockpicker">
                                        <input type="text" name="time_ramadan_debut" id="time_ramadan_debut" class="form-control" pattern="[0-2]{1}[0-9]{1}:[0-5]{1}[0-9]{1}" value="08:00" required> <span class="input-group-addon"> <span class="glyphicon glyphicon-time"></span> </span>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="time_ramadan_fin" class="col-12 col-form-label">Heure Fin (*)</label>
                                <div class="col-12">
                                    <div class="input-group clockpicker">
                                        <input type="text" name="time_ramadan_fin" id="time_ramadan_fin" class="form-control" pattern="[0-2]{1}[0-9]{1}:[0-5]{1}[0-9]{1}" value="10:00" required> <span class="input-group-addon"> <span class="glyphicon glyphicon-time"></span> </span>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>





                    <button type="submit" class="btn btn-success waves-effect waves-light m-r-10">Enregisrer</button>
                    <a href="{{ route('horaires.index') }}" class="btn btn-inverse waves-effect waves-light">Annuler</a>
                </form>
            </div>
        </div>
    </div>
    <!-- /.row -->

@endsection

@section('scripts')
    <script src="{{ asset('backend/eliteadmin/js/custom.min.js') }}"></script>
    <script src="{{ asset('backend/eliteadmin/js/validator.js') }}"></script>

    <!-- Clock Plugin JavaScript -->
    <script src="{{ asset('backend/plugins/bower_components/clockpicker/dist/jquery-clockpicker.min.js') }}"></script>


    <script>
        $(document).ready(function() {

            $('.clockpicker').clockpicker({
                placement: 'top',
                align: 'left',
                donetext: 'Done',
                autoclose: true
            });


        });
    </script>

@endsection