@extends('layouts.app')

@section('title', 'Les horaires')

@section('style')


@endsection

@section('content')

    @if(Session::has('success'))
        <div class="alert alert-success alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            {{ Session::get('success') }} </div>
    @endif

    <!-- .row -->
    <div class="row">
        <div class="col-sm-12">
            <div class="white-box">
                <div class="table-responsive">
                    <table class="table table-bordered color-bordered-table inverse-bordered-table">
                        <thead>
                        <tr>
                            <th>H:m</th>
                            @foreach( $jours as $key_j => $jour )
                            <th>{{ $jour }}</th>
                            @endforeach
                        </tr>
                        </thead>
                        <tbody>
                        @php

                        @endphp

                        @php

                            $interval = array();

                            foreach ($jours as $key_j => $jour) {
                                $interval[$key_j]['min'] = null;
                                $interval[$key_j]['max'] = null;
                            }

                        @endphp

                        @while ($timestamp <= $timestamp_final)
                            <tr>

                                @if( date('i', $timestamp) == 0)
                                    <td class="une_heure">{{ date('H:i', $timestamp) }}</td>
                                @else
                                    <td class="une_demi_heure">{{ date('H:i', $timestamp) }}</td>
                                @endif


                                @foreach( $jours as $key_j => $jour )

                                    @if (isset($tables_horaire[$key_j]))

                                        @if (isset($tables_horaire[$key_j][date('H:i', $timestamp)]))

                                            @if(date('H:i', $timestamp) == $tables_horaire[$key_j][date('H:i', $timestamp)]['time_hiver_debut'])

                                                    @php
                                                        $interval[$key_j]['min'] = $tables_horaire[$key_j][date('H:i', $timestamp)]['time_hiver_debut'];
                                                        $interval[$key_j]['max'] = $tables_horaire[$key_j][date('H:i', $timestamp)]['time_hiver_fin'];
                                                    @endphp

                                                    <td rowspan="{{ $tables_horaire[$key_j][date('H:i', $timestamp)]['rows'] }}" class="text-center">
                                                        <div class="time pull-right">
                                                            <a href="{{ route('horaires.edit', $tables_horaire[$key_j][date('H:i', $timestamp)]['id']) }}" data-toggle="tooltip" data-original-title="Modifier"> <i class="fa fa-pencil text-inverse m-r-10"></i> </a>
                                                            <a href="javascript:void(0);" data-url="{{ route('horaires.destroy', $tables_horaire[$key_j][date('H:i', $timestamp)]['id']) }}" data-id="{{ $tables_horaire[$key_j][date('H:i', $timestamp)]['id'] }}" data-token="{{ csrf_token() }}" data-toggle="tooltip" data-original-title="Supprimer" class="sa-warning"> <i class="fa fa-close text-danger"></i> </a>
                                                        </div>
                                                        <h2>{{ $tables_horaire[$key_j][date('H:i', $timestamp)]['code'] }}</h2>
                                                        <h4>{{ $tables_horaire[$key_j][date('H:i', $timestamp)]['nom'] }}</h4>
                                                        <h5>Horaires d'hiver :</h5>
                                                        <span class="label label-info label-rouded">{{ $tables_horaire[$key_j][date('H:i', $timestamp)]['time_hiver_debut'] }}
                                                            --> {{ $tables_horaire[$key_j][date('H:i', $timestamp)]['time_hiver_fin'] }}</span>
                                                        <h5>Horaires d'été :</h5>
                                                        <span class="label label-danger label-rouded">{{ $tables_horaire[$key_j][date('H:i', $timestamp)]['time_ete_debut'] }}
                                                            --> {{ $tables_horaire[$key_j][date('H:i', $timestamp)]['time_ete_fin'] }}</span>
                                                        <h5>Horaires de Ramadhan :</h5>
                                                        <span class="label label-warning label-rouded">{{ $tables_horaire[$key_j][date('H:i', $timestamp)]['time_ramadan_debut'] }}
                                                            --> {{ $tables_horaire[$key_j][date('H:i', $timestamp)]['time_ramadan_fin'] }}</span>

                                                    </td>



                                            @endif

                                        @else

                                                @if(  $timestamp > strtotime($interval[$key_j]['min'])
                                                 && $timestamp < strtotime($interval[$key_j]['max'])  )

                                                @else
                                                    <td></td>
                                                @endif



                                        @endif


                                    @else
                                        <td></td>
                                    @endif

                                @endforeach
                            </tr>
                            @php
                                $timestamp = $timestamp + 0.5*60*60;
                            @endphp
                        @endwhile

</tbody>
</table>

                </div>
            </div>
        </div>
    </div>
<!-- /.row -->

<!-- .row -->
<div class="row">
<div class="col-sm-12">
<a href="{{ route('horaires.create') }}" class="btn btn-info btn-md btn-block">Ajouter un horaire</a>
</div>

</div>
<!-- /.row -->



@endsection

@section('scripts')
<script src="{{ asset('backend/eliteadmin/js/custom.min.js') }}"></script>

<script>
$(document).ready(function() {

//Warning Message
$('.sa-warning').click(function(){
var id = $(this).data("id");
var token = $(this).data("token");
var url = $(this).data("url");

swal({
title: "Êtes-vous sûr?",
text: "Vous ne pourrez pas récupérer cet horaire!",
type: "warning",
showCancelButton: true,
confirmButtonColor: "#DD6B55",
confirmButtonText: "Oui, supprimez-le!",
closeOnConfirm: false
}, function(){

var _dataElemen = {
_token : token,
_method: "DELETE"
};


$.ajax({
type: 'DELETE',
url: url,
data: _dataElemen,
dataType: 'json',
success: function(data) {

    if(data.status == 'success') {
        location.reload();

        swal("Supprimé!", "Votre horaire a été supprimé.", "success");
    } else {
        swal("Oops...", data.message, "error");
    }


},
error: function(data){
    //console.log(data);
}
});
});
});

});
</script>

@endsection