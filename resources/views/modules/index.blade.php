@extends('layouts.app')

@section('title', 'Les modules')

@section('style')



@endsection

@section('content')

    @if(Session::has('success'))
        <div class="alert alert-success alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            {{ Session::get('success') }} </div>
    @endif

    <!-- .row -->
    <div class="row">
        <div class="col-sm-12">
            <div class="white-box">
                <div class="table-responsive">
                    <table class="table table-bordered table-hover color-bordered-table purple-bordered-table">
                        <thead>
                        <tr>
                            <th>Type de modue</th>
                            <th>Code Module</th>
                            <th class="text-nowrap">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach( $modules as $module)
                        <tr id="element_{{ $module->id }}">
                            <td>{{ $module->titre }}</td>
                            <td>{{ $module->code }}</td>
                            <td class="text-nowrap">
                                <a href="{{ route('modules.edit', $module->id) }}" data-toggle="tooltip" data-original-title="Modifier"> <i class="fa fa-pencil text-inverse m-r-10"></i> </a>
                                <a href="javascript:void(0);" data-url="{{ route('modules.destroy', $module->id) }}" data-id="{{ $module->id }}" data-token="{{ csrf_token() }}" data-toggle="tooltip" data-original-title="Supprimer" class="sa-warning"> <i class="fa fa-close text-danger"></i> </a>
                            </td>
                        </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- /.row -->

    <!-- .row -->
    <div class="row">
        <div class="col-sm-12">
            <a href="{{ route('modules.create') }}" class="btn btn-info btn-md btn-block">Ajouter un module</a>
        </div>

    </div>
    <!-- /.row -->



@endsection

@section('scripts')
    <script src="{{ asset('backend/eliteadmin/js/custom.min.js') }}"></script>

    <script>
        $(document).ready(function() {

            //Warning Message
            $('.sa-warning').click(function(){
                var id = $(this).data("id");
                var token = $(this).data("token");
                var url = $(this).data("url");

                swal({
                    title: "Êtes-vous sûr?",
                    text: "Vous ne pourrez pas récupérer ce module!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Oui, supprimez-le!",
                    closeOnConfirm: false
                }, function(){

                    var _dataElemen = {
                        _token : token,
                        _method: "DELETE"
                    };


                    $.ajax({
                        type: 'DELETE',
                        url: url,
                        data: _dataElemen,
                        dataType: 'json',
                        success: function(data) {

                            if(data.status == 'success') {
                                $('#element_'+id).remove();

                                swal("Supprimé!", "Votre module a été supprimé.", "success");
                            } else {
                                swal("Oops...", data.message, "error");
                            }


                        },
                        error: function(data){
                            //console.log(data);
                        }
                    });
                });
            });

        });
    </script>

@endsection