<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    @include('layouts.head')


</head>

<body>
    @include('layouts.preloader')

    <div id="wrapper" class="error-page">

        @yield('content')


    </div>
    <!-- /#wrapper -->
    @include('layouts.script')
    <!-- Custom Theme JavaScript -->
    @yield('scripts')
</body>
</html>
