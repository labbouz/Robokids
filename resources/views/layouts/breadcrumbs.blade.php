

@switch(request()->route()->getName())
@case('modules.edit')
{!! Breadcrumbs::render('modules.edit', $module) !!}
@break

@case('niveaux.edit')
{!! Breadcrumbs::render('niveaux.edit', $module) !!}
@break

@case('groupes.edit')
{!! Breadcrumbs::render('groupes.edit', $groupe) !!}
@break

@case('salles.edit')
{!! Breadcrumbs::render('salles.edit', $salle) !!}
@break

@case('horaires.edit')
{!! Breadcrumbs::render('horaires.edit', $horaire) !!}
@break

@case('formations.edit')
{!! Breadcrumbs::render('formations.edit', $formation) !!}
@break

@case('enfants.edit')
{!! Breadcrumbs::render('enfants.edit', $enfant) !!}
@break

@case('classes.index')
{!! Breadcrumbs::render('classes.index', $formation) !!}
@break

@case('classes.determine')
{!! Breadcrumbs::render('classes.determine', $formation) !!}
@break

@case('classes.creat')
{!! Breadcrumbs::render('classes.creat', $formation) !!}
@break

@case('classes.edit')
{!! Breadcrumbs::render('classes.edit', $classe) !!}
@break

@case('inscriptions.create')
{!! Breadcrumbs::render('inscriptions.create', $formation) !!}
@break

@case('abonnements.list')
{!! Breadcrumbs::render('abonnements.list', $formation) !!}
@break

@case('abonnements.show')
{!! Breadcrumbs::render('abonnements.show', $abonnement) !!}
@break

@case('abonnements.info')
{!! Breadcrumbs::render('abonnements.show', $abonnement) !!}
@break

@case('abonnements.paiements')
{!! Breadcrumbs::render('abonnements.paiements', $abonnement) !!}
@break
@case('abonnements.enfant')
{!! Breadcrumbs::render('abonnements.show', $abonnement) !!}
@break
@case('abonnements.presences')
{!! Breadcrumbs::render('abonnements.show', $abonnement) !!}
@break

@case('paiements.list')
{!! Breadcrumbs::render('paiements.list', $formation) !!}
@break

@case('paiements.filter')
{!! Breadcrumbs::render('paiements.list', $formation) !!}
@break

@default
{!! Breadcrumbs::render() !!}
@endswitch