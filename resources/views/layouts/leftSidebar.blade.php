    <!-- Left navbar-header -->
        <div class="navbar-default sidebar" role="navigation">
            <div class="sidebar-nav navbar-collapse slimscrollsidebar">

                <ul class="nav" id="side-menu">

                    <li><a href="{{ route('home') }}" class="waves-effect"><i data-icon="v" class="linea-icon linea-basic fa-fw"></i> <span class="hide-menu">Tableaux de bord</span></a> </li>

                    @widget('FormationMenus')
                    <li><a href="{{ route('enfants.index') }}" class="waves-effect"><i class="fa fa-users fa-fw"></i> <span class="hide-menu">Les enfants</span></a> </li>
                    <li><a href="{{ route('formations.index') }}" class="waves-effect"><i class="fa fa-graduation-cap fa-fw"></i> <span class="hide-menu">Les sessions</span></a> </li>
                    <li><a href="{{ route('horaires.index') }}" class="waves-effect"><i data-icon="b" class="linea-icon linea-basic fa-fw"></i> <span class="hide-menu">Les horaires</span></a> </li>
                    <li><a href="{{ route('salles.index') }}" class="waves-effect"><i class="fa fa-building-o fa-fw"></i> <span class="hide-menu">Les salles</span></a> </li>
                    <li><a href="{{ route('groupes.index') }}" class="waves-effect"><i class="fa fa-bank fa-fw"></i> <span class="hide-menu">Les groupes</span></a> </li>
                    <li><a href="{{ route('niveaux.index') }}" class="waves-effect"><i data-icon="v" class="linea-icon linea-software fa-fw"></i> <span class="hide-menu">Les niveaux</span></a> </li>
                    <li><a href="{{ route('modules.index') }}" class="waves-effect"><i data-icon="7" class="linea-icon linea-basic fa-fw"></i> <span class="hide-menu">Les modules</span></a> </li>








                    <li><a href="{{ route('logout') }}" class="waves-effect" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();"><i class="icon-logout fa-fw"></i> <span class="hide-menu">Déconnexion</span></a></li>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>

                </ul>
            </div>
        </div>
        <!-- Left navbar-header end -->