<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    @include('layouts.head')


</head>

<body>
    @include('layouts.preloader')

    <div id="wrapper">
        @include('layouts.header')

        @include('layouts.leftSidebar')
        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">

                @include('layouts.title')

                @if(Session::has('flash_message'))
                    <div class="row">
                        <div class="col-md-12">
                            <div class="alert alert-success"><em> {!! session('flash_message') !!}</em>
                            </div>
                        </div>
                    </div>
                @endif


                @yield('content')

                @include('layouts.rightSidebar')
            </div>
            <!-- /.container-fluid -->
            @include('layouts.footer')
        </div>
        <!-- /#page-wrapper -->
    </div>
    <!-- /#wrapper -->
    @include('layouts.script')
    <!-- Custom Theme JavaScript -->
    @yield('scripts')
</body>
</html>
