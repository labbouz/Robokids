@extends('layouts.app')

@section('title', 'Les paiements pour la session '.$formation->nom)

@section('style')
    <link href="{{ asset('backend/plugins/bower_components/datatables/jquery.dataTables.min.css') }}" rel="stylesheet">

    <style>

        .fa-active-1 {
            color: #00a300;
            font-size: 18px !important;
        }

        .fa-active-1:before {
            content: "\f00c";
        }

        .fa-clock-o {
            color: #ffa200;
            font-size: 18px !important;
        }

        .fa-active-0 {
            color: #ff0000;
            font-size: 18px !important;
        }

        .fa-active-0:before {
            content: "\f00d";
        }

        .active-col .fa-active-1,
        .active-col .fa-clock-o,
        .active-col .fa-active-0 {
            color: #ffffff;
        }

    </style>

@endsection

@section('content')

    @if(Session::has('success'))
        <div class="alert alert-success alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            {{ Session::get('success') }} </div>
    @endif

    <!-- .row -->
    <div class="row">
        <div class="col-sm-12">
            <div class="white-box">
                <form class="form" method="POST" action="{{ route('paiements.filter', $formation->id) }}">
                    {{ csrf_field() }}

                    <div class="form-group row">
                        <label for="niveau_id" class="col-2 col-form-label text-right">Niveau</label>
                        <div class="col-4">
                            <select class="form-control custom-select col-12 firlter-abonnement" name="niveau_id" id="niveau_id" data-minlength="1" required>
                                @php
                                    $all_niveau_selected = '';
                                    if(!$niveau_id_selected) {
                                        $all_niveau_selected = ' selected';
                                    }
                                @endphp
                                <option  value="" {{ $all_niveau_selected }}>Tous les niveaux</option>
                                @foreach($niveaux as $niveau)
                                    @php
                                        $item_niveau_selected = '';
                                        if($niveau->id == $niveau_id_selected) {
                                            $item_niveau_selected = ' selected';
                                        }
                                    @endphp
                                    <option value="{{ $niveau->id }}" {{ $item_niveau_selected }}>{{ $niveau->module->titre }} --> {{ $niveau->nom }}</option>
                                @endforeach
                            </select>
                        </div>
                        <label for="classe_id" class="col-2 col-form-label text-right">Classe </label>
                        <div class="col-4">
                            <select class="form-control custom-select col-12 firlter-abonnement" name="classe_id" id="classe_id" data-minlength="1" required>
                                @php
                                    $all_classe_selected = '';
                                    if(!$classe_id_selected) {
                                        $all_classe_selected = ' selected';
                                    }
                                @endphp
                                <option  value="" {{ $all_classe_selected }}>Toutes les classes</option>
                                @foreach($classes as $classe)
                                    @php
                                        $item_classe_selected = '';
                                        if($classe->id == $classe_id_selected) {
                                            $item_classe_selected = ' selected';
                                        }
                                    @endphp
                                    <option value="{{ $classe->id }}" {{ $item_classe_selected }}>{{ $classe->groupe->nom }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>


                </form>
            </div>
        </div>
    </div>
    <!-- /.row -->

    <!-- .row -->
    <div class="row">
        <div class="col-sm-12">
            <div class="white-box">
                <div class="table-responsive">
                    <table id="table-groupes" class="table table-bordered display">
                        <thead>
                        <tr>
                            <th>Enfant</th>
                            @foreach($dates_months as $date_month)
                                @php
                                    $class_current_month_active = '';
                                    if($date_month['mois_courant']) {
                                        $class_current_month_active = 'active-col';
                                    }
                                @endphp

                                <th class="text-center {{ $class_current_month_active }}">
                                    <big>
                                        {{ $date_month['mois'] }}
                                    </big>
                                    <br>
                                    <small>
                                    {{ $date_month['anne'] }}
                                    </small>
                                </th>
                            @endforeach

                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach( $abonnements as $abonnement)
                        <tr id="element_{{ $abonnement->id }}">
                            <td>{{ $abonnement->enfant->prenom }}  {{  $abonnement->enfant->nom }}</td>
                            @foreach($dates_months as $date_month)
                                @php
                                    $class_current_month_active = '';
                                    if($date_month['mois_courant']) {
                                        $class_current_month_active = 'active-col';
                                    }
                                @endphp
                            <td class="text-center {{ $class_current_month_active }}">
                                @php
                                    $datas_paiements = $abonnement->datas_paiements;
                                @endphp

                                @if( array_key_exists($date_month['anne'] . '_' . $date_month['mois'], $datas_paiements) )

                                    @if($datas_paiements[$date_month['anne'].'_'.$date_month['mois']]['status_paiement'] == 1)
                                        <i class="fa fa-active-1"></i></a>
                                    @elseif($datas_paiements[$date_month['anne'].'_'.$date_month['mois']]['status_paiement'] == 0)
                                        <i class="fa fa-clock-o"></i></a>
                                    @else
                                        <i class="fa fa-active-0"></i></a>
                                    @endif

                                @else

                                    @if( $date_month['anne'] < date('Y'))
                                        <i class="fa fa-active-0"></i></a>
                                    @elseif( $date_month['anne'] && $date_month['mois'] < date('n') )
                                        <i class="fa fa-active-0"></i></a>
                                    @else
                                        <i class="fa fa-clock-o"></i></a>
                                    @endif

                                @endif


                            </td>
                            @endforeach
                            <td class="text-nowrap text-center">
                                <a href="{{ route('abonnements.paiements', $abonnement->id) }}" data-toggle="tooltip" data-original-title="Détail Abonnement"> <i class="fa fa-eye text-inverse"></i> </a>
                                {{--
                                <a href="{{ route('groupes.edit', $abonnement->id) }}" data-toggle="tooltip" data-original-title="Modifier"> <i class="fa fa-pencil text-inverse m-r-10"></i> </a>
                                <a href="javascript:void(0);" data-url="{{ route('groupes.destroy', $abonnement->id) }}" data-id="{{ $abonnement->id }}" data-token="{{ csrf_token() }}" data-toggle="tooltip" data-original-title="Supprimer" class="sa-warning"> <i class="fa fa-close text-danger"></i> </a>
                                --}}
                            </td>
                        </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- /.row -->


@endsection

@section('scripts')
    <script src="{{ asset('backend/eliteadmin/js/custom.min.js') }}"></script>
    <script src="{{ asset('backend/plugins/bower_components/datatables/jquery.dataTables.min.js') }}"></script>

    <script>
        $(document).ready(function() {

            $('#table-groupes').DataTable({
                "paging":   true,
                "ordering": true,
                "info":     true,
                "language": {
                    "sProcessing": "Traitement en cours...",
                    "sSearch": "Rechercher&nbsp;:",
                    "sLengthMenu": "Afficher _MENU_ &eacute;l&eacute;ments",
                    "sInfo": "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                    "sInfoEmpty": "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
                    "sInfoFiltered": "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                    "sInfoPostFix": "",
                    "sLoadingRecords": "Chargement en cours...",
                    "sZeroRecords": "Aucun &eacute;l&eacute;ment &agrave; afficher",
                    "sEmptyTable": "Aucune donn&eacute;e disponible dans le tableau",
                    "oPaginate": {
                        "sFirst": "Premier",
                        "sPrevious": "Pr&eacute;c&eacute;dent",
                        "sNext": "Suivant",
                        "sLast": "Dernier"
                    },
                    "oAria": {
                        "sSortAscending": ": activer pour trier la colonne par ordre croissant",
                        "sSortDescending": ": activer pour trier la colonne par ordre d&eacute;croissant"
                    },
                },
                "columns": [
                    { "orderable": true },
                    @foreach($dates_months as $date_month)
                    { "orderable": false },
                    @endforeach
                    { "orderable": false }
                ],
                /*
                "columnDefs": [
                    {
                        "targets": [ 1 ],
                        "visible": false,
                        "searchable": true
                    },
                    {
                        "targets": [ 2 ],
                        "visible": false,
                        "searchable": true
                    },
                    {
                        "targets": [ 3 ],
                        "visible": false,
                        "searchable": true
                    }
                ]
                */
            });


            //Warning Message
            $('.sa-warning').click(function(){
                var id = $(this).data("id");
                var token = $(this).data("token");
                var url = $(this).data("url");

                swal({
                    title: "Êtes-vous sûr?",
                    text: "Vous ne pourrez pas récupérer ce groupe!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Oui, supprimez-le!",
                    closeOnConfirm: false
                }, function(){

                    var _dataElemen = {
                        _token : token,
                        _method: "DELETE"
                    };


                    $.ajax({
                        type: 'DELETE',
                        url: url,
                        data: _dataElemen,
                        dataType: 'json',
                        success: function(data) {

                            $('#element_'+id).remove();

                            swal("Supprimé!", "Votre niveau a été supprimé.", "success");
                        },
                        error: function(data){
                            //console.log(data);

                            swal("Supprimé!", "Votre niveau a été supprimé.", "success");
                        }
                    });
                });
            });

        });


        $('.firlter-abonnement').change(function() {
            this.form.submit();
        });
    </script>

@endsection