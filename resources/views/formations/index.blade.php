@extends('layouts.app')

@section('title', 'Les sessions de formation')

@section('style')



@endsection

@section('content')

    @if(Session::has('success'))
        <div class="alert alert-success alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            {{ Session::get('success') }} </div>
    @endif

    <!-- .row -->
    <div class="row">
        <div class="col-sm-12">
            <div class="white-box">
                <div class="table-responsive">
                    <table class="table table-bordered table-hover color-bordered-table purple-bordered-table">
                        <thead>
                        <tr>
                            <th>Nom de Session </th>
                            <th>Date début session</th>
                            <th>Date fin session</th>
                            <th>Session Courante</th>
                            <th class="text-nowrap">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach( $formations as $formation)
                        <tr id="element_{{ $formation->id }}">
                            <td>{{ $formation->nom }}</td>
                            <td>{{ \Carbon\Carbon::createFromFormat('Y-m-d', $formation->date_debut)->formatLocalized('%d/%m/%Y') }}</td>
                            <td>{{ \Carbon\Carbon::createFromFormat('Y-m-d', $formation->date_fin)->formatLocalized('%d/%m/%Y') }}</td>
                            <td>

                                @if($formation->active)
                                <span class="label label-success label-rounded">active</span>
                                @else
                                <span class="label label-danger label-rounded">désactive</span>
                                @endif

                            </td>
                            <td class="text-nowrap">
                                <a href="{{ route('formations.edit', $formation->id) }}" data-toggle="tooltip" data-original-title="Modifier"> <i class="fa fa-pencil text-inverse m-r-10"></i> </a>
                                <a href="javascript:void(0);" data-url="{{ route('formations.destroy', $formation->id) }}" data-id="{{ $formation->id }}" data-token="{{ csrf_token() }}" data-toggle="tooltip" data-original-title="Supprimer" class="sa-warning"> <i class="fa fa-close text-danger"></i> </a>
                            </td>
                        </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- /.row -->

    <!-- .row -->
    <div class="row">
        <div class="col-sm-12">
            <a href="{{ route('formations.create') }}" class="btn btn-info btn-md btn-block">Ajouter une sessions de formation</a>
        </div>

    </div>
    <!-- /.row -->



@endsection

@section('scripts')
    <script src="{{ asset('backend/eliteadmin/js/custom.min.js') }}"></script>

    <script>
        $(document).ready(function() {

            //Warning Message
            $('.sa-warning').click(function(){
                var id = $(this).data("id");
                var token = $(this).data("token");
                var url = $(this).data("url");

                swal({
                    title: "Êtes-vous sûr?",
                    text: "Vous ne pourrez pas récupérer cette session de formation!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Oui, supprimez-le!",
                    closeOnConfirm: false
                }, function(){

                    var _dataElemen = {
                        _token : token,
                        _method: "DELETE"
                    };


                    $.ajax({
                        type: 'DELETE',
                        url: url,
                        data: _dataElemen,
                        dataType: 'json',
                        success: function(data) {

                            if(data.status == 'success') {
                                $('#element_'+id).remove();

                                swal("Supprimé!", "Votre session a été supprimé.", "success");
                            } else {
                                swal("Oops...", data.message, "error");
                            }


                        },
                        error: function(data){
                            //console.log(data);
                        }
                    });
                });
            });

        });
    </script>

@endsection