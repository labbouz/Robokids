@extends('layouts.app')

@section('title', 'Ajouter une session de formation')

@section('style')

    <link href="{{ asset('backend/plugins/bower_components/switchery/dist/switchery.min.css') }}" rel="stylesheet">

    <!-- Date picker plugins css -->
    <link href="{{ asset('backend/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}" rel="stylesheet">
    <!-- Daterange picker plugins css -->
    <link href="{{ asset('backend/plugins/bower_components/timepicker/bootstrap-timepicker.min.css') }}" rel="stylesheet">
    <link href="{{ asset('backend/plugins/bower_components/bootstrap-daterangepicker/daterangepicker.css') }}" rel="stylesheet">

@endsection

@section('content')




    <!-- .row -->
    <div class="row">
        <div class="col-sm-12">
            <div class="white-box">
                <h3 class="box-title m-b-0">Formulaire de création une nouvelle de sessions de formation</h3>
                <p class="text-muted m-b-30 font-13"> Tous les champs en (*) sont obligatoires </p>

                @if ($errors->any())

                    @foreach ($errors->all() as $error)
                        <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            {{ $error }} </div>
                    @endforeach

                @endif

                <form class="form" method="POST" action="{{ route('formations.store') }}" data-toggle="validator">
                    {{ csrf_field() }}

                    <div class="form-group row">
                        <label for="nom" class="col-2 col-form-label">Nom session (*)</label>
                        <div class="col-10">
                            <input class="form-control" type="text" value="" id="nom" name="nom" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="date_debut" class="col-2 col-form-label">Date session (*)</label>
                        <div class="col-10">
                            <div class="input-daterange input-group" id="date-range">
                                <input type="text" class="form-control" id="date_debut" name="date_debut" required />
                                <span class="input-group-addon bg-info b-0 text-white">to</span>
                                <input type="text" class="form-control" id="date_fin" name="date_fin" required /> </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="active" class="col-2 col-form-label">Session courante ?</label>
                        <div class="col-10">
                            <input name="active" id="active" type="checkbox" class="js-switch" data-color="#99d683" value="1" />
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="frais_inscription" class="col-2 col-form-label">Frais d'inscription</label>
                        <div class="col-2">
                            <div class="input-group">
                                <input type="text" class="form-control" value="10.000" pattern="^[0-9]{1,7}(\.[0-9]+)?$" id="frais_inscription" name="frais_inscription" required>
                                <span class="input-group-addon">TND</span>
                            </div>

                        </div>

                    </div>

                    <div class="form-group row">
                        <label for="frais_session" class="col-2 col-form-label">Frais de session</label>
                        <div class="col-2">
                            <div class="input-group">
                                <input type="text" class="form-control" value="40.000" pattern="^[0-9]{1,7}(\.[0-9]+)?$" id="frais_session" name="frais_session" required>
                                <span class="input-group-addon">TND</span>
                            </div>

                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="pourcentage_double" class="col-2 col-form-label">Remise 2 classes</label>
                        <div class="col-2">
                            <div class="input-group">
                                <input type="text" class="form-control" value="12.5" pattern="^[0-9]{1,7}(\.[0-9]+)?$" id="pourcentage_double" name="pourcentage_double" required>
                                <span class="input-group-addon">%</span>
                            </div>

                        </div>
                    </div>
					
					<div class="form-group row">
                        <label for="pourcentage_triple" class="col-2 col-form-label">Remise 3 classes</label>
                        <div class="col-2">
                            <div class="input-group">
                                <input type="text" class="form-control" value="20" pattern="^[0-9]{1,7}(\.[0-9]+)?$" id="pourcentage_triple" name="pourcentage_triple" required>
                                <span class="input-group-addon">%</span>
                            </div>

                        </div>
                    </div>

                    <button type="submit" class="btn btn-success waves-effect waves-light m-r-10">Enregisrer</button>
                    <a href="{{ route('formations.index') }}" class="btn btn-inverse waves-effect waves-light">Annuler</a>
                </form>
            </div>
        </div>
    </div>
    <!-- /.row -->

@endsection

@section('scripts')
    <script src="{{ asset('backend/eliteadmin/js/custom.min.js') }}"></script>
    <script src="{{ asset('backend/eliteadmin/js/validator.js') }}"></script>

    <script src="{{ asset('backend/plugins/bower_components/switchery/dist/switchery.min.js') }}"></script>

    <!-- Date Picker Plugin JavaScript -->
    <script src="{{ asset('backend/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('backend/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.fr.min.js') }}"></script>
    <!-- Date range Plugin JavaScript -->
    <script src="{{ asset('backend/plugins/bower_components/timepicker/bootstrap-timepicker.min.js') }}"></script>
    <script src="{{ asset('backend/plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>

    <script>
        $(document).ready(function() {
            // Switchery
            var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
            $('.js-switch').each(function() {
                new Switchery($(this)[0], $(this).data());
            });

            jQuery('#date-range').datepicker({
                toggleActive: true,
                language: "fr",
                format: "yyyy-mm-dd"
            });

        });
    </script>

@endsection