@extends('layouts.app')

@section('title', 'Modifier la salle ' . $salle->nom )

@section('style')



@endsection

@section('content')




    <!-- .row -->
    <div class="row">
        <div class="col-sm-12">
            <div class="white-box">
                <h3 class="box-title m-b-0">Formulaire de modification d'une salle</h3>
                <p class="text-muted m-b-30 font-13"> Tous les champs en (*) sont obligatoires </p>

                @if ($errors->any())

                    @foreach ($errors->all() as $error)
                        <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            {{ $error }} </div>
                    @endforeach

                @endif

                <form class="form" method="POST" action="{{ route('salles.update', $salle->id) }}" data-toggle="validator">
                    {{ csrf_field() }}
                    {{ method_field('PATCH') }}

                    <div class="form-group row">
                        <label for="nom" class="col-2 col-form-label">Nom (*)</label>
                        <div class="col-10">
                            <input class="form-control" type="text" value="{{ $salle->nom }}" id="nom" name="nom" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="code" class="col-2 col-form-label">Code (*)</label>
                        <div class="col-10">
                            <input class="form-control" type="text" value="{{ $salle->code }}" id="code" name="code" required>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-success waves-effect waves-light m-r-10">Enregisrer</button>
                    <a href="{{ route('salles.index') }}" class="btn btn-inverse waves-effect waves-light">Annuler</a>
                </form>
            </div>
        </div>
    </div>
    <!-- /.row -->

@endsection

@section('scripts')
    <script src="{{ asset('backend/eliteadmin/js/custom.min.js') }}"></script>
    <script src="{{ asset('backend/eliteadmin/js/validator.js') }}"></script>

    <script>
        $(document).ready(function() {


        });
    </script>

@endsection