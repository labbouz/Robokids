@extends('layouts.app')

@section('title', 'Répartition les classes de la session ' . $formation->nom)

@section('style')


@endsection

@section('content')

    @if(Session::has('success'))
        <div class="alert alert-success alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            {{ Session::get('success') }} </div>
    @endif


    @php
        $timestamp = strtotime($time_initial);
        $timestamp_final = strtotime($time_final);
        $tables_horaire = $tables_horaire_hiver;
    @endphp

    <!-- .row -->
    <div class="row">
        <div class="col-sm-12">
            <div class="white-box">
                <h3 class="box-title">Horaires d'hiver</h3>
                <div class="table-responsive">
                    <table class="table table-bordered color-bordered-table info-bordered-table table-determine-classes">
                        <thead>
                        <tr>
                            <th>H:m</th>
                            @foreach( $jours as $key_j => $jour )
                                @if (isset($tables_horaire[$key_j]))
                                    <th>{{ $jour }}</th>
                                @endif
                            @endforeach
                        </tr>
                        </thead>
                        <tbody>
                        @php
                            $interval = array();
                            foreach ($jours as $key_j => $jour) {
                                $interval[$key_j]['min'] = null;
                                $interval[$key_j]['max'] = null;
                            }
                        @endphp

                        @while ($timestamp <= $timestamp_final)
                            <tr>
                                @if( date('i', $timestamp) == 0)
                                    <td class="une_heure">{{ date('H:i', $timestamp) }}</td>
                                @else
                                    <td class="une_demi_heure">{{ date('H:i', $timestamp) }}</td>
                                @endif

                                @foreach( $jours as $key_j => $jour )
                                    @if (isset($tables_horaire[$key_j]))
                                        @if (isset($tables_horaire[$key_j][date('H:i', $timestamp)]))
                                            @if(date('H:i', $timestamp) == $tables_horaire[$key_j][date('H:i', $timestamp)]['time_debut'])
                                                @php
                                                    $interval[$key_j]['min'] = $tables_horaire[$key_j][date('H:i', $timestamp)]['time_debut'];
                                                    $interval[$key_j]['max'] = $tables_horaire[$key_j][date('H:i', $timestamp)]['time_fin'];
                                                @endphp

                                                <td rowspan="{{ $tables_horaire[$key_j][date('H:i', $timestamp)]['rows'] }}" class="time-horaire">
                                                    <span class="horaire">{{ $tables_horaire[$key_j][date('H:i', $timestamp)]['code'] }}</span>
                                                    <div class="container-espaces-salles">

                                                        @php
                                                        $horaire_id = $tables_horaire[$key_j][date('H:i', $timestamp)]['id'];
                                                        @endphp

                                                        @foreach( $salles as $salle)

                                                            @if(array_key_exists($horaire_id.':'.$salle->id, $classes))
                                                                @php
                                                                $classe = $classes[$horaire_id.':'.$salle->id];
                                                                @endphp

                                                                <a href="{{ route('classes.edit', $classe->id) }}" class="espaces-salles classe-existe">

                                                                    <span class="nom-classe">{{ $classe->groupe->nom }}</span>
                                                                    <span class="local-salle">{{ $salle->code }}</span>


                                                                </a>
                                                              @else

                                                                <a href="{{ route('classes.creat', [$formation->id,$horaire_id,$salle->id]) }}" class="espaces-salles">

                                                                    <span class="nom-classe">dispo</span>
                                                                    <span class="local-salle">{{ $salle->code }}</span>


                                                                </a>


                                                            @endif


                                                            @endforeach
                                                    </div>


                                                </td>
                                            @endif
                                        @else
                                            @if(  $timestamp > strtotime($interval[$key_j]['min'])
                                             && $timestamp < strtotime($interval[$key_j]['max'])  )

                                            @else
                                                <td></td>
                                            @endif
                                        @endif
                                    @endif
                                @endforeach
                            </tr>
                            @php
                                $timestamp = $timestamp + 0.5*60*60;
                            @endphp
                        @endwhile

                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
    <!-- /.row -->

@endsection

@section('scripts')
<script src="{{ asset('backend/eliteadmin/js/custom.min.js') }}"></script>

<script>
$(document).ready(function() {

    //Warning Message
    $('.sa-warning').click(function(){
        var id = $(this).data("id");
        var token = $(this).data("token");
        var url = $(this).data("url");

        swal({
            title: "Êtes-vous sûr?",
            text: "Vous ne pourrez pas récupérer cet horaire!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Oui, supprimez-le!",
            closeOnConfirm: false
            }, function(){

                var _dataElemen = {
                    _token : token,
                    _method: "DELETE"
                };


                $.ajax({
                    type: 'DELETE',
                    url: url,
                    data: _dataElemen,
                    dataType: 'json',
                    success: function(data) {

                        if(data.status == 'success') {
                            location.reload();

                            swal("Supprimé!", "Votre horaire a été supprimé.", "success");
                        } else {
                            swal("Oops...", data.message, "error");
                        }


                    },
                    error: function(data){
                        //console.log(data);
                    }
                });
            });
    });

});
</script>

@endsection