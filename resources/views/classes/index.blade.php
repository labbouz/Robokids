@extends('layouts.app')

@section('title', 'Tableau des horaires de la session '.$formation->nom)

@section('style')

    <style type="text/css">

        .tableau-not-active {
            display: none;
        }

        .tableau-active {
            display: block !important;
        }

    </style>

@endsection

@section('content')

    @if(Session::has('success'))
        <div class="alert alert-success alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            {{ Session::get('success') }} </div>
    @endif

    <div class="row">
        <div class="col-lg-12">
            <div class="white-box button-box">
                <div class="row">
                    <div class="col-lg-4 col-sm-12 col-xs-12">
                        <button class="btn btn-block btn-info active-table" data-table="h-hiver">HORAIRES D'HIVER</button>
                    </div>
                    <div class="col-lg-4 col-sm-12 col-xs-12">
                        <button class="btn btn-block btn-danger active-table" data-table="h-ete">HORAIRES D'ÉTÉ</button>
                    </div>
                    <div class="col-lg-4 col-sm-12 col-xs-12">
                        <button class="btn btn-block btn-warning active-table" data-table="h-ramadhan">HORAIRES DE RAMADHAN</button>
                    </div>
                </div>
            </div>
        </div>
    </div>



    @php
        $timestamp = strtotime($time_initial);
        $timestamp_final = strtotime($time_final);
        $tables_horaire = $tables_horaire_hiver;
    @endphp

    <!-- .row -->
    <div id="h-hiver" class="row tableau-not-active tableau-active">
        <div class="col-sm-12">
            <div class="white-box">
                <h3 class="box-title">Horaires d'hiver</h3>
                <div class="table-responsive">
                    <table class="table table-bordered color-bordered-table info-bordered-table table-determine-classes">
                        <thead>
                        <tr>
                            <th>H:m</th>
                            @foreach( $jours as $key_j => $jour )
                                @if (isset($tables_horaire[$key_j]))
                                    <th>{{ $jour }}</th>
                                @endif
                            @endforeach
                        </tr>
                        </thead>
                        <tbody>
                        @php
                            $interval = array();
                            foreach ($jours as $key_j => $jour) {
                                $interval[$key_j]['min'] = null;
                                $interval[$key_j]['max'] = null;
                            }
                        @endphp

                        @while ($timestamp <= $timestamp_final)
                            <tr>
                                @if( date('i', $timestamp) == 0)
                                    <td class="une_heure">{{ date('H:i', $timestamp) }}</td>
                                @else
                                    <td class="une_demi_heure">{{ date('H:i', $timestamp) }}</td>
                                @endif

                                @foreach( $jours as $key_j => $jour )
                                    @if (isset($tables_horaire[$key_j]))
                                        @if (isset($tables_horaire[$key_j][date('H:i', $timestamp)]))
                                            @if(date('H:i', $timestamp) == $tables_horaire[$key_j][date('H:i', $timestamp)]['time_debut'])
                                                @php
                                                    $interval[$key_j]['min'] = $tables_horaire[$key_j][date('H:i', $timestamp)]['time_debut'];
                                                    $interval[$key_j]['max'] = $tables_horaire[$key_j][date('H:i', $timestamp)]['time_fin'];
                                                @endphp

                                                <td rowspan="{{ $tables_horaire[$key_j][date('H:i', $timestamp)]['rows'] }}" class="time-horaire-display">
                                                    <span class="horaire">{{ $tables_horaire[$key_j][date('H:i', $timestamp)]['code'] }}</span>
                                                    <div class="couche-selection"></div>

                                                    @php
                                                        $horaire_id = $tables_horaire[$key_j][date('H:i', $timestamp)]['id'];
                                                    @endphp

                                                    @foreach( $salles as $salle)

                                                        @if(array_key_exists($horaire_id.':'.$salle->id, $classes))
                                                            @php
                                                                $classe = $classes[$horaire_id.':'.$salle->id];
                                                            @endphp

                                                            <a href="#" class="espaces-salles
                                                                @if($classe->abonnements_active->count() >= $classe->max_childrens )
                                                                    espaces-salles-complet
                                                                @endif
                                                            ">
                                                                <!-- espaces-salles-complet -->

                                                                <span class="nom-classe">{{ $classe->groupe->nom }}</span>
                                                                <br>
                                                                <span class="local-salle">{{ $salle->nom }}</span>
                                                                <br>
                                                                <span class="nombre-inscris">{{ $classe->abonnements_active->count() }}/{{ $classe->max_childrens }} <i class="fa fa-child"></i></span>


                                                            </a>
                                                        @endif

                                                    @endforeach
                                                </td>
                                            @endif
                                        @else
                                            @if(  $timestamp > strtotime($interval[$key_j]['min'])
                                             && $timestamp < strtotime($interval[$key_j]['max'])  )

                                            @else
                                                <td></td>
                                            @endif
                                        @endif
                                    @endif
                                @endforeach
                            </tr>
                            @php
                                $timestamp = $timestamp + 0.5*60*60;
                            @endphp
                        @endwhile

                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
    <!-- /.row -->

    @php
        $timestamp = strtotime($time_initial);
        $timestamp_final = strtotime($time_final);
        $tables_horaire = $tables_horaire_ete;
    @endphp

    <!-- .row -->
    <div id="h-ete" class="row tableau-not-active">
        <div class="col-sm-12">
            <div class="white-box">
                <h3 class="box-title">Horaires d'été</h3>
                <div class="table-responsive">
                    <table class="table table-bordered color-bordered-table danger-bordered-table table-determine-classes">
                        <thead>
                        <tr>
                            <th>H:m</th>
                            @foreach( $jours as $key_j => $jour )
                                @if (isset($tables_horaire[$key_j]))
                                    <th>{{ $jour }}</th>
                                @endif
                            @endforeach
                        </tr>
                        </thead>
                        <tbody>
                        @php
                            $interval = array();
                            foreach ($jours as $key_j => $jour) {
                                $interval[$key_j]['min'] = null;
                                $interval[$key_j]['max'] = null;
                            }
                        @endphp

                        @while ($timestamp <= $timestamp_final)
                            <tr>
                                @if( date('i', $timestamp) == 0)
                                    <td class="une_heure">{{ date('H:i', $timestamp) }}</td>
                                @else
                                    <td class="une_demi_heure">{{ date('H:i', $timestamp) }}</td>
                                @endif

                                @foreach( $jours as $key_j => $jour )
                                    @if (isset($tables_horaire[$key_j]))
                                        @if (isset($tables_horaire[$key_j][date('H:i', $timestamp)]))
                                            @if(date('H:i', $timestamp) == $tables_horaire[$key_j][date('H:i', $timestamp)]['time_debut'])
                                                @php
                                                    $interval[$key_j]['min'] = $tables_horaire[$key_j][date('H:i', $timestamp)]['time_debut'];
                                                    $interval[$key_j]['max'] = $tables_horaire[$key_j][date('H:i', $timestamp)]['time_fin'];
                                                @endphp

                                                <td rowspan="{{ $tables_horaire[$key_j][date('H:i', $timestamp)]['rows'] }}" class="time-horaire-display">
                                                    <span class="horaire">{{ $tables_horaire[$key_j][date('H:i', $timestamp)]['code'] }}</span>
                                                    <div class="couche-selection"></div>

                                                    @php
                                                        $horaire_id = $tables_horaire[$key_j][date('H:i', $timestamp)]['id'];
                                                    @endphp

                                                    @foreach( $salles as $salle)

                                                        @if(array_key_exists($horaire_id.':'.$salle->id, $classes))
                                                            @php
                                                                $classe = $classes[$horaire_id.':'.$salle->id];
                                                            @endphp

                                                            <a href="#" class="espaces-salles
                                                                @if($classe->abonnements_active->count() >= $classe->max_childrens )
                                                                    espaces-salles-complet
                                                                @endif
                                                                    ">
                                                                <!-- espaces-salles-complet -->

                                                                <span class="nom-classe">{{ $classe->groupe->nom }}</span>
                                                                <br>
                                                                <span class="local-salle">{{ $salle->nom }}</span>
                                                                <br>
                                                                <span class="nombre-inscris">{{ $classe->abonnements_active->count() }}/{{ $classe->max_childrens }} <i class="fa fa-child"></i></span>


                                                            </a>
                                                        @endif

                                                    @endforeach
                                                </td>
                                            @endif
                                        @else
                                            @if(  $timestamp > strtotime($interval[$key_j]['min'])
                                             && $timestamp < strtotime($interval[$key_j]['max'])  )

                                            @else
                                                <td></td>
                                            @endif
                                        @endif
                                    @endif
                                @endforeach
                            </tr>
                            @php
                                $timestamp = $timestamp + 0.5*60*60;
                            @endphp
                        @endwhile

                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
    <!-- /.row -->

    @php
        $timestamp = strtotime($time_initial);
        $timestamp_final = strtotime($time_final);
        $tables_horaire = $tables_horaire_ramadan;
    @endphp

    <!-- .row -->
    <div id="h-ramadhan" class="row tableau-not-active">
        <div class="col-sm-12">
            <div class="white-box">
                <h3 class="box-title">Horaires de Ramadhan</h3>
                <div class="table-responsive">
                    <table class="table table-bordered color-bordered-table warning-bordered-table table-determine-classes">
                        <thead>
                        <tr>
                            <th>H:m</th>
                            @foreach( $jours as $key_j => $jour )
                                @if (isset($tables_horaire[$key_j]))
                                    <th>{{ $jour }}</th>
                                @endif
                            @endforeach
                        </tr>
                        </thead>
                        <tbody>
                        @php
                            $interval = array();
                            foreach ($jours as $key_j => $jour) {
                                $interval[$key_j]['min'] = null;
                                $interval[$key_j]['max'] = null;
                            }
                        @endphp

                        @while ($timestamp <= $timestamp_final)
                            <tr>
                                @if( date('i', $timestamp) == 0)
                                    <td class="une_heure">{{ date('H:i', $timestamp) }}</td>
                                @else
                                    <td class="une_demi_heure">{{ date('H:i', $timestamp) }}</td>
                                @endif

                                @foreach( $jours as $key_j => $jour )
                                    @if (isset($tables_horaire[$key_j]))
                                        @if (isset($tables_horaire[$key_j][date('H:i', $timestamp)]))
                                            @if(date('H:i', $timestamp) == $tables_horaire[$key_j][date('H:i', $timestamp)]['time_debut'])
                                                @php
                                                    $interval[$key_j]['min'] = $tables_horaire[$key_j][date('H:i', $timestamp)]['time_debut'];
                                                    $interval[$key_j]['max'] = $tables_horaire[$key_j][date('H:i', $timestamp)]['time_fin'];
                                                @endphp

                                                <td rowspan="{{ $tables_horaire[$key_j][date('H:i', $timestamp)]['rows'] }}" class="time-horaire-display">
                                                    <span class="horaire">{{ $tables_horaire[$key_j][date('H:i', $timestamp)]['code'] }}</span>
                                                    <div class="couche-selection"></div>

                                                    @php
                                                        $horaire_id = $tables_horaire[$key_j][date('H:i', $timestamp)]['id'];
                                                    @endphp

                                                    @foreach( $salles as $salle)

                                                        @if(array_key_exists($horaire_id.':'.$salle->id, $classes))
                                                            @php
                                                                $classe = $classes[$horaire_id.':'.$salle->id];
                                                            @endphp

                                                            <a href="#" class="espaces-salles
                                                                @if($classe->abonnements_active->count() >= $classe->max_childrens )
                                                                    espaces-salles-complet
                                                                @endif
                                                                    ">
                                                                <!-- espaces-salles-complet -->

                                                                <span class="nom-classe">{{ $classe->groupe->nom }}</span>
                                                                <br>
                                                                <span class="local-salle">{{ $salle->nom }}</span>
                                                                <br>
                                                                <span class="nombre-inscris">{{ $classe->abonnements_active->count() }}/{{ $classe->max_childrens }} <i class="fa fa-child"></i></span>


                                                            </a>
                                                        @endif

                                                    @endforeach
                                                </td>
                                            @endif
                                        @else
                                            @if(  $timestamp > strtotime($interval[$key_j]['min'])
                                             && $timestamp < strtotime($interval[$key_j]['max'])  )

                                            @else
                                                <td></td>
                                            @endif
                                        @endif
                                    @endif
                                @endforeach
                            </tr>
                            @php
                                $timestamp = $timestamp + 0.5*60*60;
                            @endphp
                        @endwhile

                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
    <!-- /.row -->


@endsection

@section('scripts')
<script src="{{ asset('backend/eliteadmin/js/custom.min.js') }}"></script>

<script src="{{ asset('backend/plugins/bower_components/jqueryui/jquery-ui.min.js') }}"></script>


<script>
$(document).ready(function() {

    $('.active-table').click(function(){
        var _tableCible = $(this).attr('data-table');

        $('.tableau-active').hide('fade', {}, 500, function(){
            $(this).removeClass( "tableau-active", 0, function(){
                $('#'+_tableCible).addClass( "tableau-active", 0, function(){
                    $(this).show('fade', {}, 500, function(){

                    });
                });
            });




        });


    });


});
</script>

@endsection