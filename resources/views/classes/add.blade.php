@extends('layouts.app')

@section('title', 'Ajouter une classe pour la session ' . $formation->nom)

@section('style')


@endsection

@section('content')




    <!-- .row -->
    <div class="row">
        <div class="col-sm-12">
            <div class="white-box">
                <h3 class="box-title m-b-0">Formulaire de création une nouvelle classe</h3>
                <p class="text-muted m-b-30 font-13"> Tous les champs en (*) sont obligatoires </p>

                @if ($errors->any())

                    @foreach ($errors->all() as $error)
                        <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            {{ $error }} </div>
                    @endforeach

                @endif

                <form class="form" method="POST" action="{{ route('classes.store') }}" data-toggle="validator">
                    {{ csrf_field() }}

                    <input type="hidden" name="formation_id" value="{{ $formation->id }}">
                    <input type="hidden" name="horaire_id" value="{{ $horaire->id }}">
                    <input type="hidden" name="salle_id" value="{{ $salle->id }}">

                    <div class="form-group row">
                        <div class="col-4">
                            <h3>La session de formation</h3>
                            <h4>{{ $formation->nom }}</h4>
                            <h5><span class="label label-rouded label-info">{{ \Carbon\Carbon::createFromFormat('Y-m-d', $formation->date_debut)->formatLocalized('%d/%m/%Y') }}</span>  -->  <span class="label label-rouded label-info">{{ \Carbon\Carbon::createFromFormat('Y-m-d', $formation->date_fin)->formatLocalized('%d/%m/%Y') }}</span></h5>
                        </div>

                        <div class="col-4">
                            <h3>Les horaires</h3>
                            <h4>{{ $horaire->nom }} ({{ $horaire->code }})</h4>
                            <h5>Horaires d'hiver : <span class="label label-info label-rouded">{{ $horaire->time_hiver_debut }} --> {{ $horaire->time_hiver_fin }}</span></h5>
                            <h5>Horaires d'été : <span class="label label-danger label-rouded">{{ $horaire->time_ete_debut }} --> {{ $horaire->time_ete_fin }}</span></h5>
                            <h5>Horaires de Ramadhan : <span class="label label-warning label-rouded">{{ $horaire->time_ramadan_debut }} --> {{ $horaire->time_ramadan_fin }}</span></h5>
                        </div>

                        <div class="col-4">
                            <h3>La salle</h3>
                            <h4>{{ $salle->nom }}</h4>
                            <h5><span class="label label-rouded label-info">{{ $salle->code }}</span></h5>
                        </div>

                    </div>

                    <div class="form-group row">
                        <label for="groupe_id" class="col-2 col-form-label">Le groupe (*)</label>
                        <div class="col-10">
                            <select class="form-control custom-select col-12" name="groupe_id" id="groupe_id" data-minlength="1" required>
                                <option  value="" selected>Choisir...</option>
                                @foreach($groupes as $groupe)
                                    @if(!in_array($groupe->id, $goupes_existent))
                                    <option value="{{ $groupe->id }}">{{ $groupe->nom }}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="max_childrens" class="col-2 col-form-label">Nombre maximum d'enfants (*)</label>
                        <div class="col-10">
                            <input class="form-control" type="number" value="10" id="max_childrens" name="max_childrens" min="1" required>
                        </div>
                    </div>


                    <button type="submit" class="btn btn-success waves-effect waves-light m-r-10">Enregisrer</button>
                    <a href="{{ route('classes.determine', $formation->id) }}" class="btn btn-inverse waves-effect waves-light">Annuler</a>
                </form>
            </div>
        </div>
    </div>
    <!-- /.row -->

@endsection

@section('scripts')
    <script src="{{ asset('backend/eliteadmin/js/custom.min.js') }}"></script>
    <script src="{{ asset('backend/eliteadmin/js/validator.js') }}"></script>

    <!-- Clock Plugin JavaScript -->
    <script src="{{ asset('backend/plugins/bower_components/clockpicker/dist/jquery-clockpicker.min.js') }}"></script>


    <script>
        $(document).ready(function() {

            $('.clockpicker').clockpicker({
                placement: 'top',
                align: 'left',
                donetext: 'Done',
                autoclose: true
            });


        });
    </script>

@endsection