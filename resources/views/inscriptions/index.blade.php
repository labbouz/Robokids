@extends('layouts.app')

@section('title', 'Inscription')

@section('style')

    <!-- Date picker plugins css -->
    <link href="{{ asset('backend/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}" rel="stylesheet">

    <link href="{{ asset('backend/plugins/bower_components/switchery/dist/switchery.min.css') }}" rel="stylesheet">

    <!-- Wizard CSS -->
    <link href="{{ asset('backend/plugins/bower_components/jquery-wizard-master/css/wizard.css') }}" rel="stylesheet">



@endsection

@section('content')

    @if(Session::has('success'))
        <div class="alert alert-success alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            {{ Session::get('success') }} </div>
    @endif

    <form id="form-inscription" class="form" method="POST" action="{{ route('abonnements.store') }}" data-toggle="validator" enctype="multipart/form-data">
    {{ csrf_field() }}
    <!-- .row -->
    <div class="row">
        <div class="col-sm-12">
            <div class="white-box">
                <div id="formInscription" class="wizard">
                    <ul class="wizard-steps" role="tablist">
                        <li class="active" role="tab">
                            <h4><span>1</span>Classes</h4> </li>
                        <li role="tab">
                            <h4><span>2</span>Enfant</h4> </li>
                        <li role="tab">
                            <h4><span>3</span>Confirmation</h4> </li>
                    </ul>
                    <h4 class="col-sm-12 m-t-30 m-b-30 text-right btns-inscrit"></h4>
                    <div class="wizard-content">
                        <div class="wizard-pane active" role="tabpanel">

                            @php
                                $timestamp = strtotime($time_initial);
                                $timestamp_final = strtotime($time_final);
                                $tables_horaire = $tables_horaire_hiver;
                            @endphp


                            <div class="form-group row">
                                <label for="niveau_id" class="col-4 col-form-label">Selection le niveaux :</label>
                                <div class="col-8">
                                    <select class="form-control custom-select col-12" id="niveau_id" data-minlength="1" required>
                                        <option  value="0" selected>Toutes les niveaux.</option>
                                        @foreach($niveaux as $niveau)
                                            <option value="{{ $niveau->id }}">{{ $niveau->module->titre }} --> {{ $niveau->nom }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="table-responsive">
                                <table class="table table-bordered color-bordered-table info-bordered-table table-determine-classes">
                                    <thead>
                                    <tr>
                                        @foreach( $jours as $key_j => $jour )
                                            @if (isset($tables_horaire[$key_j]))
                                                <th>{{ $jour }}</th>
                                            @endif
                                        @endforeach
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @php
                                        $interval = array();
                                        foreach ($jours as $key_j => $jour) {
                                            $interval[$key_j]['min'] = null;
                                            $interval[$key_j]['max'] = null;
                                        }
                                    @endphp

                                    @while ($timestamp <= $timestamp_final)
                                        <tr>


                                            @foreach( $jours as $key_j => $jour )
                                                @if (isset($tables_horaire[$key_j]))
                                                    @if (isset($tables_horaire[$key_j][date('H:i', $timestamp)]))
                                                        @if(date('H:i', $timestamp) == $tables_horaire[$key_j][date('H:i', $timestamp)]['time_debut'])
                                                            @php
                                                                $interval[$key_j]['min'] = $tables_horaire[$key_j][date('H:i', $timestamp)]['time_debut'];
                                                                $interval[$key_j]['max'] = $tables_horaire[$key_j][date('H:i', $timestamp)]['time_fin'];
                                                            @endphp

                                                            <td class="time-horaire-display">
                                                                <span class="horaire">{{ $tables_horaire[$key_j][date('H:i', $timestamp)]['code'] }}</span>
                                                                <div class="couche-selection"></div>

                                                                @php
                                                                    $horaire_id = $tables_horaire[$key_j][date('H:i', $timestamp)]['id'];
                                                                @endphp

                                                                @foreach( $salles as $salle)

                                                                    @if(array_key_exists($horaire_id.':'.$salle->id, $classes))
                                                                        @php
                                                                            $classe = $classes[$horaire_id.':'.$salle->id];
                                                                        @endphp

                                                                        @if($classe->abonnements_active->count() < $classe->max_childrens )

                                                                            <a href="javascript:void(0);" class="espaces-salles classe-existe " data-selection="0"  data-id-classe="{{ $classe->id }}" data-groupe="{{ $classe->groupe->nom }}" data-niveau="{{ $classe->groupe->niveau_id }}">
                                                                                <!-- espaces-salles-complet -->

                                                                                <span class="nom-classe">{{ $classe->groupe->nom }}</span>



                                                                            </a>

                                                                        @endif


                                                                    @endif

                                                                @endforeach
                                                            </td>
                                                        @endif
                                                    @endif
                                                @endif
                                            @endforeach
                                        </tr>
                                        @php
                                            $timestamp = $timestamp + 0.5*60*60;
                                        @endphp
                                    @endwhile

                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        @foreach( $jours as $key_j => $jour )
                                            @if (isset($tables_horaire[$key_j]))
                                                <th>{{ $jour }}</th>
                                            @endif
                                        @endforeach
                                    </tr>
                                    </tfoot>
                                </table>

                            </div>



                        </div>
                        <div class="wizard-pane" role="tabpanel">

                            <div class="row">
                                <div class="col-sm-12 col-md-12">

                                    <h3 class="box-title m-b-0">Formulaire d'ajout un nouveau enfant sur la base Robokids </h3>
                                    <p class="text-muted m-b-30 font-13"> Tous les champs en (*) sont obligatoires </p>

                                    @if ($errors->any())

                                        @foreach ($errors->all() as $error)
                                            <div class="alert alert-danger alert-dismissable">
                                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                                {{ $error }} </div>
                                        @endforeach

                                    @endif

                                </div>

                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-6">


                                            <div class="form-group row">
                                                <label for="nom" class="col-3 col-form-label">Nom (*)</label>
                                                <div class="col-9">
                                                    <input class="form-control" type="text" value="" id="nom" name="nom" required>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="prenom" class="col-3 col-form-label">Prénom (*)</label>
                                                <div class="col-9">
                                                    <input class="form-control" type="text" value="" id="prenom" name="prenom" required>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="date_naissance" class="col-3 col-form-label">Date de naissance (*)</label>
                                                <div class="col-9">
                                                    <input class="form-control date-picker" type="text" value="" id="date_naissance" name="date_naissance" required>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="genre" class="col-3 col-form-label">Genre (*)</label>
                                                <div class="col-9">

                                                    <select class="form-control custom-select col-12" name="genre" id="genre" data-minlength="1" required>
                                                        <option  value="" selected>Choisir...</option>
                                                        @foreach($genres as $key_genre => $vlue_genre)
                                                            <option value="{{ $key_genre }}">{{ $vlue_genre }}</option>
                                                        @endforeach
                                                    </select>

                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="ecole" class="col-3 col-form-label">&Eacute;cole (*)</label>
                                                <div class="col-9">
                                                    <input class="form-control" type="text" value="" id="ecole" name="ecole" required>
                                                </div>
                                            </div>


                                        </div>

                                        <div class="col-xs-12 col-sm-12 col-md-6">
                                            <div class="row">
                                                <div class="col-sm-12 col-md-12">

                                                    <div class="form-group row">
                                                        <label for="photo" class="col-3 col-form-label">Photo</label>
                                                        <div class="col-9">
                                                            <input class="form-control" type="file" value="" id="photo" name="photo">
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-sm-12 col-md-12">

                                                    <div class="form-group row">
                                                        <label for="adresse" class="col-3 col-form-label">Adresse (*)</label>
                                                        <div class="col-9">
                                                            <input class="form-control" type="text" value="" id="adresse" name="adresse" required>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="ville" class="col-3 col-form-label">Ville</label>
                                                        <div class="col-9">
                                                            <input class="form-control" type="text" value="" id="ville" name="ville">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="code_postal" class="col-3 col-form-label">Code postle</label>
                                                        <div class="col-9">
                                                            <input class="form-control" type="number" value="" id="code_postal" name="code_postal">
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="col-md-6">

                                    <h3 class="box-title m-b-30">Informations Père </h3>
                                    <div class="form-group row">
                                        <label for="nom_pere" class="col-3 col-form-label">Nom (*)</label>
                                        <div class="col-9">
                                            <input class="form-control" type="text" value="" id="nom_pere" name="nom_pere" required>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="prenom_pere" class="col-3 col-form-label">Prénom (*)</label>
                                        <div class="col-9">
                                            <input class="form-control" type="text" value="" id="prenom_pere" name="prenom_pere" required>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="telephone_pere" class="col-3 col-form-label">Mobile (*)</label>
                                        <div class="col-9">
                                            <input class="form-control" type="number" value="" id="telephone_pere" name="telephone_pere" required>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="email_pere" class="col-3 col-form-label">Email</label>
                                        <div class="col-9">
                                            <input class="form-control" type="email" value="" id="email_pere" name="email_pere">
                                        </div>
                                    </div>

                                </div>

                                <div class="col-md-6">

                                    <h3 class="box-title m-b-30">Informations Mère </h3>
                                    <div class="form-group row">
                                        <label for="nom_mere" class="col-3 col-form-label">Nom (*)</label>
                                        <div class="col-9">
                                            <input class="form-control" type="text" value="" id="nom_mere" name="nom_mere" required>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="prenom_mere" class="col-3 col-form-label">Prénom (*)</label>
                                        <div class="col-9">
                                            <input class="form-control" type="text" value="" id="prenom_mere" name="prenom_mere" required>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="telephone_mere" class="col-3 col-form-label">Mobile (*)</label>
                                        <div class="col-9">
                                            <input class="form-control" type="number" value="" id="telephone_mere" name="telephone_mere" required>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="email_mere" class="col-3 col-form-label">Email</label>
                                        <div class="col-9">
                                            <input class="form-control" type="email" value="" id="email_mere" name="email_mere">
                                        </div>
                                    </div>

                                </div>

                            </div>

                        </div>

                        <div class="wizard-pane" role="tabpanel">

                            <div class="col-xs-12" id="listes_classes_selectionnees"></div>
                            <table id="listes_classes_selectionnees" class="table table-bordered color-table inverse-table">
                                <thead>
                                <tr>
                                    <th>Objet de paiement</th>
                                    <th class="text-center">Le frais en TND</th>
                                </tr>
                                </thead>
                                <tbody>
                                    <tr id="ligne_classe_1" class="hide">
                                        <td>
                                            Frais session pour le groupe <strong id="label_groupe_1"></strong>
                                            <input type="hidden" id="id_groupe_1" name="id_groupe_1">
                                        </td>
                                        <td class="text-center"><strong>{{ $formation->frais_session }}</strong></td>
                                    </tr>
                                    <tr id="ligne_classe_2" class="hide">
                                        <td>
                                            Frais session pour le groupe <strong id="label_groupe_2"></strong>
                                            <input type="hidden" id="id_groupe_2" name="id_groupe_2">
                                            </td>
                                        <td class="text-center"><strong>{{ $formation->frais_session }}</strong></td>
                                    </tr>
									<tr id="ligne_classe_3" class="hide">
                                        <td>
                                            Frais session pour le groupe <strong id="label_groupe_3"></strong>
                                            <input type="hidden" id="id_groupe_3" name="id_groupe_3">
                                            </td>
                                        <td class="text-center"><strong>{{ $formation->frais_session }}</strong></td>
                                    </tr>
                                    <tr id="ligne_classe_remise_2" class="hide">
                                        <td class="text-right">
                                            Frais mensuelle en totale aprés remise 2 classes ( <strong>{{ $formation->pourcentage_double }} %</strong> ) :
                                        </td>
                                        @php
                                            $caclule_prix = ( $formation->frais_session * 2 ) - ($formation->frais_session * 2 * $formation->pourcentage_double / 100 )
                                        @endphp
                                        <td class="text-center"><strong id="label_total">{{ number_format($caclule_prix,3) }}</strong></td>
                                    </tr>
									
									<tr id="ligne_classe_remise_3" class="hide">
                                        <td class="text-right">
                                            Frais mensuelle en totale aprés remise 3 classes ( <strong>{{ $formation->pourcentage_triple }} %</strong> ) :
                                        </td>
                                        @php
                                            $caclule_prix = ( $formation->frais_session * 3 ) - ($formation->frais_session * 3 * $formation->pourcentage_triple / 100 )
                                        @endphp
                                        <td class="text-center"><strong id="label_total">{{ number_format($caclule_prix,3) }}</strong></td>
                                    </tr>

                                    <tr id="ligne_classe_sans_remise" class="hide">
                                        <td class="text-right">
                                            Frais mensuelle en totale :
                                        </td>
                                        <td class="text-center"><strong id="label_total">{{ $formation->frais_session }}</strong></td>
                                    </tr>


                                    <tr>
                                        <td class="text-right">

                                            <input type="hidden" name="formation_id" value="{{ $formation->id }}">

                                            <div class="form-group row">
                                                <label for="active" class="col-10 col-form-label text-right">Frais Inscription ?</label>
                                                <div class="col-2">
                                                    <input name="frais_inscription" id="frais_inscription" type="checkbox" class="js-switch" data-color="#99d683" value="1" checked />
                                                </div>
                                            </div>

                                        </td>
                                        <td class="text-center"><strong id="label_frais_inscription">{{ $formation->frais_inscription }}</strong></td>
                                    </tr>
                                </tbody>
                            </table>


                            <input type="hidden" name="classes_ids">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.row -->

    </form>






@endsection

@section('scripts')
<script src="{{ asset('backend/eliteadmin/js/custom.min.js') }}"></script>
<script src="{{ asset('backend/eliteadmin/js/validator.js') }}"></script>

<script src="{{ asset('backend/plugins/bower_components/switchery/dist/switchery.min.js') }}"></script>

<script src="{{ asset('backend/plugins/bower_components/jqueryui/jquery-ui.min.js') }}"></script>

<!-- Form Wizard JavaScript -->
<script src="{{ asset('backend/plugins/bower_components/jquery-wizard-master/dist/jquery-wizard.min.js') }}"></script>
<!-- FormValidation -->
<link href="{{ asset('backend/plugins/bower_components/jquery-wizard-master/libs/formvalidation/formValidation.min.css') }}" rel="stylesheet">
<!-- FormValidation plugin and the class supports validating Bootstrap form -->
<script src="{{ asset('backend/plugins/bower_components/jquery-wizard-master/libs/formvalidation/formValidation.min.js') }}"></script>
<script src="{{ asset('backend/plugins/bower_components/jquery-wizard-master/libs/formvalidation/bootstrap.min.js') }}"></script>

<!-- Date Picker Plugin JavaScript -->
<script src="{{ asset('backend/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('backend/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.fr.min.js') }}"></script>

<script>
$(document).ready(function() {

    // Switchery
    var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
    $('.js-switch').each(function() {
        new Switchery($(this)[0], $(this).data());
    });

    //var classes_selectionnees = { };

    $('#formInscription').wizard({
        buttonsAppendTo: '.btns-inscrit',
        onFinish: function() {
            $( "#form-inscription" ).submit();
        },
        validator: function(step) {
            switch (step.index) {
                case 0:
                    if(controleGroupes() > 0) {
                        return true;
                    } else {
                        return false;
                    }
                    break;
                case 1:
                    if ($('#form-inscription').validator('validate').has('.has-error').length === 0) {
                        return true;
                    } else {
                        return false;
                    }
                    break;
                case 2:
                    return true;
                    break;

            }




        },
        buttonLabels: {
            next: 'Suivant',
            back: 'Précédent',
            finish: 'Confirmer'
        }
    });

    $( "#niveau_id" ).change(function() {
        var niveau_id = $(this).val();

        if(niveau_id != '0') {
            $("a.espaces-salles").each(function() {
                var selectorClasse = $(this);
                var dataClasse = selectorClasse.attr('data-niveau');

                if(dataClasse == niveau_id) {
                    selectorClasse.show();
                } else {
                    selectorClasse.hide();
                }

            });

        } else {
            $("a.espaces-salles").each(function() {
                $(this).show();
            });
        }

    });

    //$('.wizard-next').hide();



    $( "a.espaces-salles" ).click(function() {
        var selectorGroupe = $( this );
        var data_selection = selectorGroupe.attr("data-selection");


        if(data_selection == '1' ) {
            selectorGroupe.removeClass('espaces-salles-selectionee');
            selectorGroupe.attr("data-selection",'0');
            displayClasses();
        } else {
            if(controleGroupes() < 3) {

                selectorGroupe.addClass('espaces-salles-selectionee');
                selectorGroupe.attr("data-selection",'1');

                displayClasses();
            }

        }

    });

    function controleGroupes() {

        var nommre_groupe_selectionnes = 0;

        $(".espaces-salles-selectionee").each(function() {

            var data_selection = $(this).attr("data-selection");

            if(data_selection == '1' ) {

                nommre_groupe_selectionnes++;
            }

        });

        return nommre_groupe_selectionnes ;

    }


    function displayClasses() {

        $('#ligne_classe_1').addClass('hide');
        $('#ligne_classe_2').addClass('hide');
		$('#ligne_classe_3').addClass('hide');
        $('#id_groupe_1').val('0');
        $('#id_groupe_2').val('0');
		$('#id_groupe_3').val('0');
        $('#ligne_classe_remise_2').addClass('hide');
		$('#ligne_classe_remise_3').addClass('hide');
        $('#ligne_classe_sans_remise').addClass('hide');

        var indexGroupe = 1;
        $(".espaces-salles-selectionee").each(function() {

            $('#label_groupe_'+indexGroupe).text($( this ).attr("data-groupe"));
            $('#id_groupe_'+indexGroupe).val($( this ).attr("data-id-classe"));
            $('#ligne_classe_'+indexGroupe).removeClass('hide');

            indexGroupe++;
        });

        if(indexGroupe > 1) {
			if(indexGroupe == 2) {
				$('#ligne_classe_remise_2').removeClass('hide');
			} else {
				$('#ligne_classe_remise_3').removeClass('hide');
			}
        } else {
            $('#ligne_classe_sans_remise').removeClass('hide');
        }

    }


    $('#frais_inscription').change(function() {
        var fraisInscrit = '0.000'
        if(this.checked) {
            fraisInscrit = '{{ $formation->frais_inscription }}';
        }
        $('#label_frais_inscription').text(fraisInscrit);
    });

    jQuery('.date-picker').datepicker({
        toggleActive: true,
        language: "fr",
        format: "yyyy-mm-dd"
    });
});
</script>

@endsection