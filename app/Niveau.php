<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Niveau extends Model
{
    protected $table = 'niveau';

    protected $fillable = ['nom', 'rang', 'module_id'];

    public function module()
    {
        return $this->belongsTo('\App\Module');
    }

    public function groupes()
    {
        return $this->hasMany('\App\Groupe');
    }


    public function classes()
    {
        return $this->hasManyThrough('App\Calsse', 'App\Groupe');
    }

}
