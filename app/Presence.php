<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Presence extends Model
{
    protected $table = 'presences';

    protected $fillable = ['abonnement_calsse_id', 'date_presence', 'present'];

    /*
    public function formation()
    {
        return $this->belongsTo('\App\Formation');
    }

    public function enfant()
    {
        return $this->belongsTo('\App\Enfant');
    }

    public function classes()
    {
        return $this->belongsToMany('App\Calsse')->withTimestamps();
    }
    */
}
