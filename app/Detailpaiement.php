<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Detailpaiement extends Model
{
    protected $table = 'details_paiements';

    protected $fillable = ['paiement_id', 'calsse_id', 'calsse_active'];

    public function paiement()
    {
        return $this->belongsTo('\App\Paiement');
    }

}
