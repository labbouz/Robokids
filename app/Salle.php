<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Salle extends Model
{
    protected $table = 'salles';

    protected $fillable = ['nom', 'code'];


    public function classes()
    {
        return $this->hasMany('\App\Calsse');
    }
}
