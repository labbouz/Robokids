<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Paiement extends Model
{
    protected $table = 'paiements';

    protected $fillable = ['abonnement_id', 'mois_paiement', 'anne_paiement', 'montant', 'reglement'];

    public function abonnement()
    {
        return $this->belongsTo('\App\Abonnement');
    }

    public function detail_paiement()
    {
        return $this->hasMany('\App\Detailpaiement');
    }
}
