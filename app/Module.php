<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Module extends Model
{

    protected $table = 'modules';

    protected $fillable = ['titre', 'code'];

    public function niveaux()
    {
        return $this->hasMany('\App\Niveau');
    }
}
