<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Abonnement extends Model
{
    protected $table = 'abonnements';

    protected $fillable = ['formation_id', 'enfant_id', 'frais_inscription', 'date_debut_abonnement', 'date_fin_abonnement', 'status'];

    public function formation()
    {
        return $this->belongsTo('\App\Formation');
    }

    public function enfant()
    {
        return $this->belongsTo('\App\Enfant');
    }

    public function classes()
    {
        return $this->belongsToMany('App\Calsse')->withPivot('active')->withTimestamps();
    }


    public function paiements()
    {
        return $this->hasMany('\App\Paiement');
    }


}
