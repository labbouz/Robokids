<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Horaire extends Model
{
    protected $table = 'horaires';

    protected $fillable = ['nom', 'code', 'jour_en_semaine', 'time_hiver_debut', 'time_hiver_fin', 'time_ete_debut', 'time_ete_fin', 'time_ramadan_debut', 'time_ramadan_fin'];

    public function classes()
    {
        return $this->hasMany('\App\Calsse');
    }
}
