<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Horaire;

use Session;

class HoraireController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $horaires = Horaire::all();

        $jours =array(
            6 => 'SAM',
            7 => 'DIM',
            1 => 'LUN',
            2 => 'MAR',
            3 => 'MER',
            4 => 'JEU',
            5 => 'VEN'
        );

        $time_initial = '08:00';
        $time_final = '20:00';

        $timestamp = strtotime($time_initial);
        $timestamp_final = strtotime($time_final);

        $tables_horaire = array();
        foreach ($horaires as $horaire) {

            $timestamp_debut = strtotime($horaire->time_hiver_debut);
            $timestamp_fin = strtotime($horaire->time_hiver_fin);

            $defirence = ($timestamp_fin - $timestamp_debut) / (0.5*60*60);

            $tables_horaire[$horaire->jour_en_semaine][$horaire->time_hiver_debut]['rows'] = $defirence;
            $tables_horaire[$horaire->jour_en_semaine][$horaire->time_hiver_debut]['id'] = $horaire->id;
            $tables_horaire[$horaire->jour_en_semaine][$horaire->time_hiver_debut]['nom'] = $horaire->nom;
            $tables_horaire[$horaire->jour_en_semaine][$horaire->time_hiver_debut]['code'] = $horaire->code;
            $tables_horaire[$horaire->jour_en_semaine][$horaire->time_hiver_debut]['time_hiver_debut'] = $horaire->time_hiver_debut;
            $tables_horaire[$horaire->jour_en_semaine][$horaire->time_hiver_debut]['time_hiver_fin'] = $horaire->time_hiver_fin;
            $tables_horaire[$horaire->jour_en_semaine][$horaire->time_hiver_debut]['time_ete_debut'] = $horaire->time_ete_debut;
            $tables_horaire[$horaire->jour_en_semaine][$horaire->time_hiver_debut]['time_ete_fin'] = $horaire->time_ete_fin;
            $tables_horaire[$horaire->jour_en_semaine][$horaire->time_hiver_debut]['time_ramadan_debut'] = $horaire->time_ramadan_debut;
            $tables_horaire[$horaire->jour_en_semaine][$horaire->time_hiver_debut]['time_ramadan_fin'] = $horaire->time_ramadan_fin;

        }

        return view('horaires.index', compact('tables_horaire', 'jours', 'timestamp', 'timestamp_final'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $jours =array(
            1 => 'Lundi',
            2 => 'Mardi',
            3 => 'Mercredi',
            4 => 'Jeudi',
            5 => 'Vendredi',
            6 => 'Samedi',
            7 => 'Dimanche'
        );

        return view('horaires.add', compact('jours') );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, array(
            'nom' => 'required|max:255',
            'code' => 'required|max:255',
            'jour_en_semaine' => 'required|numeric|min:1',
            'time_hiver_debut' => 'required|max:255',
            'time_hiver_fin' => 'required|max:255',
            'time_ete_debut' => 'required|max:255',
            'time_ete_fin' => 'required|max:255',
            'time_ramadan_debut' => 'required|max:255',
            'time_ramadan_fin' => 'required|max:255'
        ));

        $horaire = new Horaire;
        $horaire->nom = $request->nom;
        $horaire->code = $request->code;
        $horaire->jour_en_semaine = $request->jour_en_semaine;
        $horaire->time_hiver_debut = $request->time_hiver_debut;
        $horaire->time_hiver_fin = $request->time_hiver_fin;
        $horaire->time_ete_debut = $request->time_ete_debut;
        $horaire->time_ete_fin = $request->time_ete_fin;
        $horaire->time_ramadan_debut = $request->time_ramadan_debut;
        $horaire->time_ramadan_fin = $request->time_ramadan_fin;


        $horaire->save();

        Session::flash('success', 'L\'horaire a été créé!');

        return redirect()->route('horaires.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $jours =array(
            1 => 'Lundi',
            2 => 'Mardi',
            3 => 'Mercredi',
            4 => 'Jeudi',
            5 => 'Vendredi',
            6 => 'Samedi',
            7 => 'Dimanche'
        );

        $horaire = Horaire::find($id);

        return view('horaires.edit', compact('horaire', 'jours'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $horaire = Horaire::find($id);

        $this->validate($request, array(
            'nom' => 'required|max:255',
            'code' => 'required|max:255',
            'jour_en_semaine' => 'required|numeric|min:1',
            'time_hiver_debut' => 'required|max:255',
            'time_hiver_fin' => 'required|max:255',
            'time_ete_debut' => 'required|max:255',
            'time_ete_fin' => 'required|max:255',
            'time_ramadan_debut' => 'required|max:255',
            'time_ramadan_fin' => 'required|max:255'
        ));


        $horaire->nom = $request->nom;
        $horaire->code = $request->code;
        $horaire->jour_en_semaine = $request->jour_en_semaine;
        $horaire->time_hiver_debut = $request->time_hiver_debut;
        $horaire->time_hiver_fin = $request->time_hiver_fin;
        $horaire->time_ete_debut = $request->time_ete_debut;
        $horaire->time_ete_fin = $request->time_ete_fin;
        $horaire->time_ramadan_debut = $request->time_ramadan_debut;
        $horaire->time_ramadan_fin = $request->time_ramadan_fin;


        $horaire->save();

        Session::flash('success', 'L\'horaire a été modifié!');

        return redirect()->route('horaires.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $horaire = Horaire::find($id);

        /*
        $total_elements = $module->niveaux()->count();

        $mot_element = 'niveau';
        if($total_elements > 1) {
            $mot_element .= 'x';
        }

        if ($total_elements > 0 ) {
            $response = array(
                'status' => 'error',
                'message' => 'Il y a ' . $total_elements . ' '.$mot_element.' pour ce module .... il faut enlever avant la suppression.'
            );

        } else {
            $module->delete();
            $response = array(
                'status' => 'success'
            );
        }
        */

        $horaire->delete();
        $response = array(
            'status' => 'success'
        );

        return response()->json($response);
    }
}
