<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Formation;
use App\Horaire;
use App\Calsse;
use App\Groupe;
use App\Niveau;
use App\Salle;
use App\Abonnement;

use App\Paiement;
use App\Detailpaiement;


use Carbon\Carbon;

use Date;

use Session;

class PaiementController extends Controller
{
    public function index(Request $request, $id_formation)
    {

        $formation = Formation::find($id_formation);


        $startDate = Carbon::createFromFormat('Y-m-d', $formation->date_debut)->startOfMonth();
        $endDate = Carbon::createFromFormat('Y-m-d', $formation->date_fin)->startOfMonth();
        $currentMonth = Carbon::createFromFormat('Y-m-d', date('Y-m-d'))->startOfMonth();

        $diff  = $startDate->diffInMonths($endDate);


        $theMonth = $startDate;

        $dates_months = array();
        for($m=0; $m<=$diff;$m++ ) {;


            $anne = $theMonth->year;
            $mois = $theMonth->month;

            if($theMonth == $currentMonth) {
                $mois_courant = 1;
            } else {
                $mois_courant = 0;
            }

            $info_mois = [
                'anne' => $anne,
                'mois' => $mois,
                'mois_courant' => $mois_courant
            ];


            $dates_months[] = $info_mois;

            $theMonth->addMonth();

        }






        $niveaux = Niveau::all()->sortBy("rang");

        $niveau_id_selected = $request->input('niveau_id');
        if($niveau_id_selected) {
            $niveau = Niveau::find($niveau_id_selected);

            $classes = $niveau->classes;

        } else {
            $classes = $formation->classes;
        }


        $classe_id_selected = $request->input('classe_id');

        if(!$classe_id_selected && !$niveau_id_selected ) {
            $abonnements  = $formation->abonnements;
        } else {
            if($classe_id_selected) {

                $classe = Calsse::find($classe_id_selected);
                $abonnements = $classe->abonnements;
            } else { //!$niveau_id_selected
                $first = 0;
                foreach ($niveau->classes as $class_niveau) {
                    if($first>0) {
                        $abonnements_new = $class_niveau->abonnements;
                        $abonnements = $abonnements->merge($abonnements_new);
                    } else {
                        $abonnements = $class_niveau->abonnements;
                    }

                    $first++;
                }
            }
        }

        foreach( $abonnements as $abonnement) {

            $datas_paiements = array();
            foreach ($abonnement->paiements as $paiement) {
                $datas_paiements[$paiement->anne_paiement.'_'.$paiement->mois_paiement]['id'] = $paiement->id;
                $datas_paiements[$paiement->anne_paiement.'_'.$paiement->mois_paiement]['mois_paiement'] = $paiement->mois_paiement;
                $datas_paiements[$paiement->anne_paiement.'_'.$paiement->mois_paiement]['anne_paiement'] = $paiement->anne_paiement;
                $datas_paiements[$paiement->anne_paiement.'_'.$paiement->mois_paiement]['montant'] = $paiement->montant;
                $datas_paiements[$paiement->anne_paiement.'_'.$paiement->mois_paiement]['reglement'] = $paiement->reglement;

                if($paiement->reglement) {
                    $datas_paiements[$paiement->anne_paiement.'_'.$paiement->mois_paiement]['status_paiement'] = 1;
                } else {
                    if( $paiement->anne_paiement < date('Y')) {
                        $datas_paiements[$paiement->anne_paiement.'_'.$paiement->mois_paiement]['status_paiement'] = -1;
                    } else {
                        if( $paiement->anne_paiement == date('Y') && $paiement->mois_paiement < date('n') ) {
                            $datas_paiements[$paiement->anne_paiement.'_'.$paiement->mois_paiement]['status_paiement'] = -1;
                        } else {
                            $datas_paiements[$paiement->anne_paiement.'_'.$paiement->mois_paiement]['status_paiement'] = 0;
                        }
                    }



                }
                //selon les details paiement
                $datas_paiements[$paiement->anne_paiement.'_'.$paiement->mois_paiement]['remise'] = 1;
            }

            $abonnement->datas_paiements = $datas_paiements;

        }





        return view('paiements.index', compact('formation', 'abonnements', 'dates_months', 'niveaux', 'classes', 'niveau_id_selected', 'classe_id_selected'));
    }


    public function add(Request $request)
    {
        $this->validate($request, array(
            'abonnement_id' => 'required|numeric|exists:abonnements,id',
            'mois_paiement' => 'required|numeric',
            'anne_paiement' => 'required|numeric',
            'reglement' => 'boolean'
        ));

        //Abonnement
        $abonnement = Abonnement::find($request->abonnement_id);

        $data_classes_info = array();
        $i=0;
        $total_class_actives = 0;
        foreach($abonnement->classes as $classe) {

            if($classe->pivot->active) {
                $active = true;
                $total_class_actives++;
            } else {
                if( $request->anne_paiement < $classe->pivot->updated_at->format('Y')) {
                    $active = true;
                    $total_class_actives++;
                } else {
                    if( $request->anne_paiement == $classe->pivot->updated_at->format('Y') && $request->mois_paiement < $classe->pivot->updated_at->format('n') ) {
                        $active = true;
                        $total_class_actives++;
                    } else {
                        $active = false;
                        $total_class_actives++;
                    }
                }
            }

            $data_classes_info[$i]['calsse_id'] = $classe->id;
            $data_classes_info[$i]['calsse_active'] = $active;

            $i++;
        }

        $frais_session = $abonnement->formation->frais_session;
        $pourcentage_double = $abonnement->formation->pourcentage_double;
		$pourcentage_triple = $abonnement->formation->pourcentage_triple;

		if($total_class_actives == 3 ) {
			$montant = ( $frais_session * 3 ) - ($frais_session * 3 * $pourcentage_triple / 100 );
		} else {
			if($total_class_actives == 2 ) {
				$montant = ( $frais_session * 2 ) - ($frais_session * 2 * $pourcentage_double / 100 );
			} else {
				if($total_class_actives == 1 ) {
					$montant = $frais_session;
				} else {
					$montant = 0;
				}
			}
		}
        

        $paiement = new Paiement;
        $paiement->abonnement_id = $abonnement->id;
        $paiement->mois_paiement = $request->mois_paiement;
        $paiement->anne_paiement = $request->anne_paiement;
        $paiement->montant = number_format($montant,3);
        $paiement->reglement = $request->reglement;

        $paiement->save();

        foreach($data_classes_info as $classes_info) {

            $detail_paiement = new Detailpaiement;
            $detail_paiement->paiement_id = $paiement->id;
            $detail_paiement->calsse_id = $classes_info['calsse_id'];
            $detail_paiement->calsse_active = $classes_info['calsse_active'];

            $detail_paiement->save();
        }


        $response = array(
            'status' => 'success',
            'id' => $paiement->id
        );

        return response()->json($response);
    }

    public function update(Request $request)
    {
        $this->validate($request, array(
            'id' => 'required|numeric|exists:paiements,id',
            'reglement' => 'boolean'
        ));

        //Abonnement
        $paiement = Paiement::find($request->id);
        $paiement->reglement = $request->reglement;

        $paiement->save();



        $response = array(
            'status' => 'success',
            'id' => $paiement->id
        );

        return response()->json($response);
    }

}
