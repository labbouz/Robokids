<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Module;

use Session;

class ModuleController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $modules = Module::all();

        return view('modules.index', compact('modules'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('modules.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, array(
            'titre' => 'required|unique:modules|max:255',
            'code' => 'required|unique:modules|max:255'
        ));



        $module = new Module;
        $module->titre = $request->titre;
        $module->code = $request->code;
        $module->save();

        Session::flash('success', 'Le module a été créé!');

        return redirect()->route('modules.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $module = Module::find($id);

        return view('modules.edit', compact('module'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $module = Module::find($id);

        $this->validate($request, array(
            'titre' => 'required|unique:modules,titre,' . $module->id . '|max:255',
            'code' => 'required|unique:modules,code,' . $module->id . '|max:255'
        ));

        $module->titre = $request->titre;
        $module->code = $request->code;
        $module->save();

        Session::flash('success', 'Le module a été modifié!');

        return redirect()->route('modules.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $module = Module::find($id);

        $total_elements = $module->niveaux()->count();

        $mot_element = 'niveau';
        if($total_elements > 1) {
            $mot_element .= 'x';
        }

        if ($total_elements > 0 ) {
            $response = array(
                'status' => 'error',
                'message' => 'Il y a ' . $total_elements . ' '.$mot_element.' pour ce module .... il faut enlever avant la suppression.'
            );

        } else {
            $module->delete();
            $response = array(
                'status' => 'success'
            );
        }



        return response()->json($response);
    }
}
