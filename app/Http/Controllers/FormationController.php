<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Formation;

use Session;

class FormationController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $formations = Formation::all()->sortByDesc("date_debut");

        return view('formations.index', compact('formations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('formations.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, array(
            'nom' => 'required|unique:formations|max:255',
            'date_debut' => 'required|unique:formations|date',
            'date_fin' => 'required|unique:formations|date',
            'active' => 'boolean',
            'frais_inscription' => 'regex:/^[0-9]{1,7}(\.[0-9]+)?$/',
            'frais_session' => 'regex:/^[0-9]{1,7}(\.[0-9]+)?$/',
            'pourcentage_double' => 'regex:/^[0-9]{1,7}(\.[0-9]+)?$/',
			'pourcentage_triple' => 'regex:/^[0-9]{1,7}(\.[0-9]+)?$/'
        ));

        if(intval($request->active)) {
            // update all
            Formation::where('active', '=', true)
                ->update([
                    'active' => false
                ]);
        }


        $formation = new Formation;
        $formation->nom = $request->nom;
        $formation->date_debut = $request->date_debut;
        $formation->date_fin = $request->date_fin;
        $formation->active = intval($request->active);
        $formation->frais_inscription = $request->frais_inscription;
        $formation->frais_session = $request->frais_session;
        $formation->pourcentage_double = $request->pourcentage_double;
		$formation->pourcentage_triple = $request->pourcentage_triple;
        $formation->save();

        Session::flash('success', 'La session de formation a été créée!');

        return redirect()->route('formations.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $formation = Formation::find($id);

        return view('formations.edit', compact('formation'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $formation = Formation::find($id);

        $this->validate($request, array(
            'nom' => 'required|unique:formations,nom,' . $formation->id . '|max:255',
            'date_debut' => 'required|unique:formations,date_debut,' . $formation->id . '|date',
            'date_fin' => 'required|unique:formations,date_fin,' . $formation->id . '|date',
            'active' => 'boolean',
            'frais_inscription' => 'regex:/^[0-9]{1,7}(\.[0-9]+)?$/',
            'frais_session' => 'regex:/^[0-9]{1,7}(\.[0-9]+)?$/',
            'pourcentage_double' => 'regex:/^[0-9]{1,7}(\.[0-9]+)?$/',
			'pourcentage_triple' => 'regex:/^[0-9]{1,7}(\.[0-9]+)?$/'
        ));

        if(intval($request->active)) {
            // update all
            Formation::where('active', '=', true)
                ->where('id', '<>', $formation->id )
                ->update([
                    'active' => false
                ]);
        }

        $formation->nom = $request->nom;
        $formation->date_debut = $request->date_debut;
        $formation->date_fin = $request->date_fin;
        $formation->active = intval($request->active);
        $formation->frais_inscription = $request->frais_inscription;
        $formation->frais_session = $request->frais_session;
        $formation->pourcentage_double = $request->pourcentage_double;
		$formation->pourcentage_triple = $request->pourcentage_triple;
        $formation->save();

        Session::flash('success', 'La session de formation a été modifiée!');

        return redirect()->route('formations.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $formation = Formation::find($id);

        /*
        $total_elements = $formation->niveaux()->count();

        $mot_element = 'niveau';
        if($total_elements > 1) {
            $mot_element .= 'x';
        }

        if ($total_elements > 0 ) {
            $response = array(
                'status' => 'error',
                'message' => 'Il y a ' . $total_elements . ' '.$mot_element.' pour ce module .... il faut enlever avant la suppression.'
            );

        } else {
            $module->delete();
            $response = array(
                'status' => 'success'
            );
        }
        */

        $formation->delete();
        $response = array(
            'status' => 'success'
        );

        return response()->json($response);
    }
}
