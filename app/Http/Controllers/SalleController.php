<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Salle;

use Session;

class SalleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $salles = Salle::all();

        return view('salles.index', compact('salles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('salles.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, array(
            'nom' => 'required|unique:salles|max:255',
            'code' => 'required|unique:salles|max:255'
        ));



        $salle = new Salle;
        $salle->nom = $request->nom;
        $salle->code = $request->code;
        $salle->save();

        Session::flash('success', 'La salle a été créée!');

        return redirect()->route('salles.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $salle = Salle::find($id);

        return view('salles.edit', compact('salle'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $salle = Salle::find($id);

        $this->validate($request, array(
            'nom' => 'required|unique:salles,nom,' . $salle->id . '|max:255',
            'code' => 'required|unique:salles,code,' . $salle->id . '|max:255'
        ));

        $salle->nom = $request->nom;
        $salle->code = $request->code;
        $salle->save();

        Session::flash('success', 'La salle a été modifiée!');

        return redirect()->route('salles.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $salle = Salle::find($id);

        $salle->delete();

        $response = array(
            'status' => 'success'
        );



        return response()->json($response);
    }
}
