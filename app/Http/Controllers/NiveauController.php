<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Module;
use App\Niveau;

use Session;

class NiveauController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $niveaux = Niveau::all()->sortBy("rang");

        return view('niveaux.index', compact('niveaux'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $modules = Module::all();

        return view('niveaux.add', compact('modules'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, array(
            'module_id' => 'required|numeric|exists:modules,id',
            'nom' => 'required|max:255|unique:niveau,nom,NULL,id,module_id,'.$request->module_id,
            'rang' => 'required|numeric|min:1'
        ));

        $niveau = new Niveau;
        $niveau->module_id = $request->module_id;
        $niveau->nom = $request->nom;
        $niveau->rang = $request->rang;
        $niveau->save();

        Session::flash('success', 'Le niveau a été créé!');

        return redirect()->route('niveaux.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $modules = Module::all();

        $niveau = Niveau::find($id);

        return view('niveaux.edit', compact('niveau', 'modules'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $niveau = Niveau::find($id);

        $this->validate($request, array(
            'module_id' => 'required|numeric|exists:modules,id',
            'nom' => 'required|max:255|unique:niveau,nom,'.$niveau->id.',id,module_id,'.$request->module_id,
            'rang' => 'required|numeric|min:1'
        ));

        $niveau->module_id = $request->module_id;
        $niveau->nom = $request->nom;
        $niveau->rang = $request->rang;
        $niveau->save();

        Session::flash('success', 'Le module a été modifié!');

        return redirect()->route('niveaux.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $niveau = Niveau::find($id);



        $total_elements = $niveau->groupes()->count();

        $mot_element = 'groupe';
        if($total_elements > 1) {
            $mot_element .= 's';
        }

        if ($total_elements > 0 ) {
            $response = array(
                'status' => 'error',
                'message' => 'Il y a ' . $total_elements . ' '.$mot_element.' pour ce niveau .... il faut enlever avant la suppression.'
            );

        } else {
            $niveau->delete();

            $response = array(
                'status' => 'success'
            );
        }




        return response()->json($response);
    }
}
