<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Enfant;
use App\Formation;
use App\Abonnement;
use App\Calsse;

use Carbon\Carbon;

use Route;

use Date;
use DB;
use Session;


class AbonnementController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, array(
            'nom' => 'required|string|max:255',
            'prenom' => 'required|string|max:255',
            'date_naissance' => 'required|date',
            'genre' => 'required|string|max:255',
            'ecole' => 'required|string|max:255',
            'photo' => 'nullable|image',
            'adresse' => 'required|string|max:255',
            'ville' => 'nullable|string|max:255',
            'code_postal' => 'nullable|numeric',
            'nom_pere' => 'required|string|max:255',
            'prenom_pere' => 'required|string|max:255',
            'telephone_pere' => 'required|numeric',
            'email_pere' => 'nullable|email',
            'nom_mere' => 'required|string|max:255',
            'prenom_mere' => 'required|string|max:255',
            'telephone_mere' => 'required|numeric',
            'email_mere' => 'nullable|email',

            'formation_id' => 'required|numeric|exists:formations,id',
            'frais_inscription' => 'boolean',
            'id_groupe_1' => 'required|numeric',
            'id_groupe_2' => 'numeric',
			'id_groupe_3' => 'numeric',

        ));



        $enfant = new Enfant;
        $enfant->nom = $request->nom;
        $enfant->prenom = $request->prenom;
        $enfant->date_naissance = $request->date_naissance;
        $enfant->genre = $request->genre;
        $enfant->ecole = $request->ecole;

        if ($request->hasFile('photo')) {

            $extension = $request->photo->extension();

            $file_name = 'first_' .date('Ymd_His') . '.' . $extension;

            $request->photo->move(public_path('images'), $file_name );

            $enfant->photo = 'images/' . $file_name;
        }

        $enfant->adresse = $request->adresse;
        $enfant->ville = $request->ville;
        $enfant->code_postal = $request->code_postal;

        $enfant->nom_pere = $request->nom_pere;
        $enfant->prenom_pere = $request->prenom_pere;
        $enfant->telephone_pere = $request->telephone_pere;
        $enfant->email_pere = $request->email_pere;

        $enfant->nom_mere = $request->nom_mere;
        $enfant->prenom_mere = $request->prenom_mere;
        $enfant->telephone_mere = $request->telephone_mere;
        $enfant->email_mere = $request->email_mere;

        $enfant->save();

        $formation = Formation::find($request->formation_id);

        $abonnement = new Abonnement;
        $abonnement->formation_id = $request->formation_id;
        $abonnement->enfant_id = $enfant->id;
        $abonnement->frais_inscription = intval($request->frais_inscription);
        $abonnement->date_debut_abonnement = $formation->date_debut;
        $abonnement->date_fin_abonnement = $formation->date_fin;
        $abonnement->status = 'active';
        $abonnement->save();


        $classe_1 = Calsse::find($request->id_groupe_1);
        if($classe_1) {
            $abonnement->classes()->save($classe_1, ['active' => true]);
        }

        $classe_2 = Calsse::find($request->id_groupe_2);
        if($classe_2) {
            $abonnement->classes()->save($classe_2, ['active' => true]);
        }
		
		$classe_3 = Calsse::find($request->id_groupe_3);
        if($classe_3) {
            $abonnement->classes()->save($classe_3, ['active' => true]);
        }





        Session::flash('success', "L'enfant ".$request->prenom." ".$request->nom." a été ajouté sur la base de données!");

        return redirect()->route('enfants.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $abonnement = Abonnement::find($id);

        $genres = array();
        $genres['garcon'] = 'Garçon';
        $genres['fille'] = 'Fille';

        $jours =array(
            1 => 'Lundi',
            2 => 'Mardi',
            3 => 'Mercredi',
            4 => 'Jeudi',
            5 => 'Vendredi',
            6 => 'Samedi',
            7 => 'Dimanche'
        );

        /******** determine l'ongle active  ******/

        $class_onglet = array();
        $class_onglet['enfant'] = "";
        $class_onglet['info'] = "";
        $class_onglet['paiements'] = "";
        $class_onglet['presences'] = "";

        switch (Route::currentRouteName()) {
            case 'abonnements.enfant':
                $class_onglet['enfant'] = "active";
                break;

            case 'abonnements.paiements':
                $class_onglet['paiements'] = "active";
                break;

            case 'abonnements.presences':
                $class_onglet['presences'] = "active";
                break;

            case 'abonnements.info':
            case 'abonnements.show':
            default:
                $class_onglet['info'] = "active";
                break;
        }

        /********* determiner les mois du paiement ********/

        $formation = $abonnement->formation;

        $startDate = Carbon::createFromFormat('Y-m-d', $formation->date_debut)->startOfMonth();
        $endDate = Carbon::createFromFormat('Y-m-d', $formation->date_fin)->startOfMonth();
        $currentMonth = Carbon::createFromFormat('Y-m-d', date('Y-m-d'))->startOfMonth();

        $diff  = $startDate->diffInMonths($endDate);


        $theMonth = $startDate;

        $dates_months = array();
        for($m=0; $m<=$diff;$m++ ) {;


            $anne = $theMonth->year;
            $mois = $theMonth->month;

            if($theMonth == $currentMonth) {
                $mois_courant = 1;
            } else {
                $mois_courant = 0;
            }

            $info_mois = [
                'anne' => $anne,
                'mois' => $mois,
                'mois_courant' => $mois_courant
            ];


            $dates_months[] = $info_mois;

            $theMonth->addMonth();

        }

        $datas_paiements = array();
        foreach ($abonnement->paiements as $paiement) {
            $datas_paiements[$paiement->anne_paiement.'_'.$paiement->mois_paiement]['id'] = $paiement->id;
            $datas_paiements[$paiement->anne_paiement.'_'.$paiement->mois_paiement]['mois_paiement'] = $paiement->mois_paiement;
            $datas_paiements[$paiement->anne_paiement.'_'.$paiement->mois_paiement]['anne_paiement'] = $paiement->anne_paiement;
            $datas_paiements[$paiement->anne_paiement.'_'.$paiement->mois_paiement]['montant'] = $paiement->montant;
            $datas_paiements[$paiement->anne_paiement.'_'.$paiement->mois_paiement]['reglement'] = $paiement->reglement;

            if($paiement->reglement) {
                $datas_paiements[$paiement->anne_paiement.'_'.$paiement->mois_paiement]['status_paiement'] = 1;
            } else {
                if( $paiement->anne_paiement < date('Y')) {
                    $datas_paiements[$paiement->anne_paiement.'_'.$paiement->mois_paiement]['status_paiement'] = -1;
                } else {
                    if( $paiement->anne_paiement == date('Y') && $paiement->mois_paiement < date('n') ) {
                        $datas_paiements[$paiement->anne_paiement.'_'.$paiement->mois_paiement]['status_paiement'] = -1;
                    } else {
                        $datas_paiements[$paiement->anne_paiement.'_'.$paiement->mois_paiement]['status_paiement'] = 0;
                    }
                }



            }



            //selon les details paiement
            $datas_paiements[$paiement->anne_paiement.'_'.$paiement->mois_paiement]['remise'] = 1;
        }

        $data_classes_info = array();
        $i=0;
        foreach($abonnement->classes as $classe) {

            $data_classes_info[$i]['id'] = $classe->id;
            $data_classes_info[$i]['nom'] = $classe->groupe->nom;
            $data_classes_info[$i]['active'] = $classe->pivot->active;

            if($classe->pivot->active) {
                $data_classes_info[$i]['mois_fin_abonnement_class'] = 0;
                $data_classes_info[$i]['anne_fin_abonnement_class'] = 0;
            } else {
                $data_classes_info[$i]['mois_fin_abonnement_class'] = $classe->pivot->updated_at->format('n');
                $data_classes_info[$i]['anne_fin_abonnement_class'] = $classe->pivot->updated_at->format('Y');
            }

            $i++;
        }

        foreach($dates_months as $date_month) {
            if( !array_key_exists($date_month['anne'] . '_' . $date_month['mois'], $datas_paiements) ) {
                $datas_paiements[$date_month['anne'].'_'.$date_month['mois']]['id'] = 0;
                $datas_paiements[$date_month['anne'].'_'.$date_month['mois']]['mois_paiement'] = $date_month['mois'];
                $datas_paiements[$date_month['anne'].'_'.$date_month['mois']]['anne_paiement'] = $date_month['anne'];


                $total_class_actives = 0;
                foreach($data_classes_info as $classes_info) {

                    if($classes_info['active']) {
                        $total_class_actives++;
                    } else {
                        if( $date_month['anne'] < $classes_info['anne_fin_abonnement_class']) {
                            $total_class_actives++;
                        } else {
                            if( $date_month['anne'] == $classes_info['anne_fin_abonnement_class'] && $date_month['mois'] < $classes_info['mois_fin_abonnement_class'] ) {
                                $total_class_actives++;
                            }
                        }
                    }
                }

				if($total_class_actives == 3 ) {
					$montant = ( $formation->frais_session * 3 ) - ($formation->frais_session * 3 * $formation->pourcentage_triple / 100 );
					$remise = 3;
				} else {
					if($total_class_actives == 2 ) {
						$montant = ( $formation->frais_session * 2 ) - ($formation->frais_session * 2 * $formation->pourcentage_double / 100 );
						$remise = 2;
					} else {
						$remise = 0;
						if($total_class_actives == 1 ) {
							$montant = $formation->frais_session;
						} else {
							$montant = 0;
						}
					}
				}
                
                $datas_paiements[$date_month['anne'].'_'.$date_month['mois']]['reglement'] = 0;
                $datas_paiements[$date_month['anne'].'_'.$date_month['mois']]['montant'] = number_format($montant,3);
                $datas_paiements[$date_month['anne'].'_'.$date_month['mois']]['remise'] = $remise;

                if( $date_month['anne'] < date('Y')) {
                    $datas_paiements[$date_month['anne'].'_'.$date_month['mois']]['status_paiement'] = -1;

                } else {
                    if( $date_month['anne'] == date('Y') && $date_month['mois'] < date('n') ) {
                        $datas_paiements[$date_month['anne'].'_'.$date_month['mois']]['status_paiement'] = -1;
                    } else {
                        $datas_paiements[$date_month['anne'].'_'.$date_month['mois']]['status_paiement'] = 0;
                    }
                }
            }
        }

        /********* ./determiner les mois du paiement ********/

        return view('abonnements.show', compact('abonnement', 'formation', 'dates_months', 'genres', 'jours', 'datas_paiements', 'data_classes_info', 'class_onglet'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
		$abonnement = Abonnement::find($id);

		foreach ($abonnement->paiements as $paiement) {
            $paiement->delete();
		}
		
		DB::table('abonnement_calsse')->where('abonnement_id', '=', $abonnement->id)->delete();
		
        $abonnement->delete();

        $response = array(
            'status' => 'success'
        );



        return response()->json($response);
    }

    /**
     * @param Request $request
     * @param $abonnement_id
     * @param $calsse_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function desactiver_class(Request $request, $abonnement_id, $calsse_id)
    {
        $abonnement = Abonnement::find($abonnement_id);

        $classe = Calsse::find($calsse_id);

        if($request->_type_action) {
            $active = true;
        } else {
            $active = false;
        }

        if($classe) {
            $abonnement->classes()->updateExistingPivot($calsse_id, ['active' => $active]);
        }

        $response = array(
            'status' => 'success'
        );

        return response()->json($response);
    }
}
