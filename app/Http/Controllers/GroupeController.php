<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Niveau;
use App\Groupe;

use Session;

class GroupeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $groupes = Groupe::all();

        return view('groupes.index', compact('groupes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $niveaux = Niveau::all()->sortBy("rang");

        return view('groupes.add', compact('niveaux'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, array(
            'niveau_id' => 'required|numeric|exists:niveau,id',
            'nom' => 'required|max:255|unique:groupes,nom,NULL,id,niveau_id,'.$request->niveau_id
        ));

        $groupe = new Groupe;
        $groupe->niveau_id = $request->niveau_id;
        $groupe->nom = $request->nom;
        $groupe->save();

        Session::flash('success', 'Le groupe a été créé!');

        return redirect()->route('groupes.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $niveaux = Niveau::all();

        $groupe = Groupe::find($id);

        return view('groupes.edit', compact('groupe', 'niveaux'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $groupe = Groupe::find($id);

        $this->validate($request, array(
            'niveau_id' => 'required|numeric|exists:niveau,id',
            'nom' => 'required|max:255|unique:groupes,nom,'.$groupe->id.',id,niveau_id,'.$request->niveau_id
        ));

        $groupe->niveau_id = $request->niveau_id;
        $groupe->nom = $request->nom;
        $groupe->save();

        Session::flash('success', 'Le groupe a été modifié!');

        return redirect()->route('groupes.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $groupe = Groupe::find($id);
        $groupe->delete();

        $response = array(
            'status' => 'success'
        );

        return response()->json($response);
    }
}
