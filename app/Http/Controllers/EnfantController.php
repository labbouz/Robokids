<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Enfant;

use Session;

class EnfantController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response = =
     */
    public function index()
    {
        $enfants = Enfant::all()->sortByDesc("id");

        return view('enfants.index', compact('enfants'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $genres = array();
        $genres['garcon'] = 'Garçon';
        $genres['fille'] = 'Fille';

        return view('enfants.add', compact('genres'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, array(
            'nom' => 'required|string|max:255',
            'prenom' => 'required|string|max:255',
            'date_naissance' => 'required|date',
            'genre' => 'required|string|max:255',
            'ecole' => 'required|string|max:255',
            'photo' => 'nullable|image',
            'adresse' => 'required|string|max:255',
            'ville' => 'nullable|string|max:255',
            'code_postal' => 'nullable|numeric',
            'nom_pere' => 'required|string|max:255',
            'prenom_pere' => 'required|string|max:255',
            'telephone_pere' => 'required|numeric',
            'email_pere' => 'nullable|email',
            'nom_mere' => 'required|string|max:255',
            'prenom_mere' => 'required|string|max:255',
            'telephone_mere' => 'required|numeric',
            'email_mere' => 'nullable|email',

        ));



        $enfant = new Enfant;
        $enfant->nom = $request->nom;
        $enfant->prenom = $request->prenom;
        $enfant->date_naissance = $request->date_naissance;
        $enfant->genre = $request->genre;
        $enfant->ecole = $request->ecole;

        if ($request->hasFile('photo')) {

            $extension = $request->photo->extension();

            $file_name = 'first_' .date('Ymd_His') . '.' . $extension;

            $request->photo->move(public_path('images'), $file_name );

            $enfant->photo = 'images/' . $file_name;
        }

        $enfant->adresse = $request->adresse;
        $enfant->ville = $request->ville;
        $enfant->code_postal = $request->code_postal;

        $enfant->nom_pere = $request->nom_pere;
        $enfant->prenom_pere = $request->prenom_pere;
        $enfant->telephone_pere = $request->telephone_pere;
        $enfant->email_pere = $request->email_pere;

        $enfant->nom_mere = $request->nom_mere;
        $enfant->prenom_mere = $request->prenom_mere;
        $enfant->telephone_mere = $request->telephone_mere;
        $enfant->email_mere = $request->email_mere;

        $enfant->save();

        Session::flash('success', "L'enfant ".$request->prenom." ".$request->nom." a été ajouté sur la base de données!");

        return redirect()->route('enfants.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $enfant = Enfant::find($id);

        $genres = array();
        $genres['garcon'] = 'Garçon';
        $genres['fille'] = 'Fille';

        return view('enfants.edit', compact('enfant', 'genres'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, array(
            'nom' => 'required|string|max:255',
            'prenom' => 'required|string|max:255',
            'date_naissance' => 'required|date',
            'genre' => 'required|string|max:255',
            'ecole' => 'required|string|max:255',
            'photo' => 'nullable|image',
            'adresse' => 'required|string|max:255',
            'ville' => 'nullable|string|max:255',
            'code_postal' => 'nullable|numeric',
            'nom_pere' => 'required|string|max:255',
            'prenom_pere' => 'required|string|max:255',
            'telephone_pere' => 'required|numeric',
            'email_pere' => 'nullable|email',
            'nom_mere' => 'required|string|max:255',
            'prenom_mere' => 'required|string|max:255',
            'telephone_mere' => 'required|numeric',
            'email_mere' => 'nullable|email',

        ));


        $enfant = Enfant::find($id);
        $enfant->nom = $request->nom;
        $enfant->prenom = $request->prenom;
        $enfant->date_naissance = $request->date_naissance;
        $enfant->genre = $request->genre;
        $enfant->ecole = $request->ecole;

        $enfant->adresse = $request->adresse;
        $enfant->ville = $request->ville;
        $enfant->code_postal = $request->code_postal;

        $enfant->nom_pere = $request->nom_pere;
        $enfant->prenom_pere = $request->prenom_pere;
        $enfant->telephone_pere = $request->telephone_pere;
        $enfant->email_pere = $request->email_pere;

        $enfant->nom_mere = $request->nom_mere;
        $enfant->prenom_mere = $request->prenom_mere;
        $enfant->telephone_mere = $request->telephone_mere;
        $enfant->email_mere = $request->email_mere;

        if ($request->hasFile('photo')) {


            // delet old file
            if($enfant->photo) {
                $filename = public_path().'/'.$enfant->photo;
                unlink($filename);
            }

            $extension = $request->photo->extension();

            $file_name = $enfant->id . '_' .date('Ymd_His') . '.' . $extension;

            $request->photo->move(public_path('images'), $file_name );

            $enfant->photo = 'images/' . $file_name;
        }

        $enfant->save();


        Session::flash('success', "La mise à jour a été faite!");

        return redirect()->route('enfants.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $enfant = Enfant::find($id);

        $enfant->delete();

        $response = array(
            'status' => 'success'
        );



        return response()->json($response);
    }
}
