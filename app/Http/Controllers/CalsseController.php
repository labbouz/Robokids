<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Formation;
use App\Horaire;
use App\Calsse;
use App\Groupe;

use App\Salle;

use Session;

class CalsseController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function sessionActive()
    {
        $formation_active = Formation::where('active', true)->first();

        return redirect()->route('classes.index', $formation_active->id);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index($id_session)
    {
        /*
         * Parti récupération les horaires générales
         */
        $horaires = Horaire::all();

        $jours =array(
            6 => 'SAM',
            7 => 'DIM',
            1 => 'LUN',
            2 => 'MAR',
            3 => 'MER',
            4 => 'JEU',
            5 => 'VEN'
        );

        $time_initial = '08:00';
        $time_final = '19:00';



        $tables_horaire = array();
        foreach ($horaires as $horaire) {


            $defirence_hiver = (strtotime($horaire->time_hiver_fin) - strtotime($horaire->time_hiver_debut)) / (0.5*60*60);
            $defirence_ete = (strtotime($horaire->time_ete_fin) - strtotime($horaire->time_ete_debut)) / (0.5*60*60);
            $defirence_ramadan = (strtotime($horaire->time_ramadan_fin) - strtotime($horaire->time_ramadan_debut)) / (0.5*60*60);

            $tables_horaire_hiver[$horaire->jour_en_semaine][$horaire->time_hiver_debut]['rows'] = $defirence_hiver;
            $tables_horaire_hiver[$horaire->jour_en_semaine][$horaire->time_hiver_debut]['id'] = $horaire->id;
            $tables_horaire_hiver[$horaire->jour_en_semaine][$horaire->time_hiver_debut]['nom'] = $horaire->nom;
            $tables_horaire_hiver[$horaire->jour_en_semaine][$horaire->time_hiver_debut]['code'] = $horaire->code;
            $tables_horaire_hiver[$horaire->jour_en_semaine][$horaire->time_hiver_debut]['time_debut'] = $horaire->time_hiver_debut;
            $tables_horaire_hiver[$horaire->jour_en_semaine][$horaire->time_hiver_debut]['time_fin'] = $horaire->time_hiver_fin;

            $tables_horaire_ete[$horaire->jour_en_semaine][$horaire->time_ete_debut]['rows'] = $defirence_ete;
            $tables_horaire_ete[$horaire->jour_en_semaine][$horaire->time_ete_debut]['id'] = $horaire->id;
            $tables_horaire_ete[$horaire->jour_en_semaine][$horaire->time_ete_debut]['nom'] = $horaire->nom;
            $tables_horaire_ete[$horaire->jour_en_semaine][$horaire->time_ete_debut]['code'] = $horaire->code;
            $tables_horaire_ete[$horaire->jour_en_semaine][$horaire->time_ete_debut]['time_debut'] = $horaire->time_ete_debut;
            $tables_horaire_ete[$horaire->jour_en_semaine][$horaire->time_ete_debut]['time_fin'] = $horaire->time_ete_fin;

            $tables_horaire_ramadan[$horaire->jour_en_semaine][$horaire->time_ramadan_debut]['rows'] = $defirence_ramadan;
            $tables_horaire_ramadan[$horaire->jour_en_semaine][$horaire->time_ramadan_debut]['id'] = $horaire->id;
            $tables_horaire_ramadan[$horaire->jour_en_semaine][$horaire->time_ramadan_debut]['nom'] = $horaire->nom;
            $tables_horaire_ramadan[$horaire->jour_en_semaine][$horaire->time_ramadan_debut]['code'] = $horaire->code;
            $tables_horaire_ramadan[$horaire->jour_en_semaine][$horaire->time_ramadan_debut]['time_debut'] = $horaire->time_ramadan_debut;
            $tables_horaire_ramadan[$horaire->jour_en_semaine][$horaire->time_ramadan_debut]['time_fin'] = $horaire->time_ramadan_fin;

        }

        $formation = Formation::find($id_session);

        /**** parti recuperer les salles ***/
        $salles = Salle::all();

        /**** parti recuperer les classe existent ***/
        $classes_currents = Calsse::where('formation_id', $formation->id )->get();
        $classes = array();
        foreach ($classes_currents as $classe_current) {
            $classes[$classe_current->horaire_id.':'.$classe_current->salle_id] = $classe_current;
        }

        return view('classes.index', compact('tables_horaire_hiver', 'tables_horaire_ete', 'tables_horaire_ramadan', 'jours', 'time_initial', 'time_final', 'salles', 'classes', 'formation'));

    }


    public function determine($id_session=null)
    {
        if($id_session) {
            $formation = Formation::find($id_session);
        } else {
            $formation_active = Formation::where('active', true)->first();

            return redirect()->route('classes.determine', $formation_active->id);
        }


        /*
         * Parti récupération les horaires générales
         */
        $horaires = Horaire::all();

        $jours =array(
            6 => 'SAM',
            7 => 'DIM',
            1 => 'LUN',
            2 => 'MAR',
            3 => 'MER',
            4 => 'JEU',
            5 => 'VEN'
        );

        $time_initial = '08:00';
        $time_final = '19:00';

        foreach ($horaires as $horaire) {


            $defirence_hiver = (strtotime($horaire->time_hiver_fin) - strtotime($horaire->time_hiver_debut)) / (0.5*60*60);

            $tables_horaire_hiver[$horaire->jour_en_semaine][$horaire->time_hiver_debut]['rows'] = $defirence_hiver;
            $tables_horaire_hiver[$horaire->jour_en_semaine][$horaire->time_hiver_debut]['id'] = $horaire->id;
            $tables_horaire_hiver[$horaire->jour_en_semaine][$horaire->time_hiver_debut]['nom'] = $horaire->nom;
            $tables_horaire_hiver[$horaire->jour_en_semaine][$horaire->time_hiver_debut]['code'] = $horaire->code;
            $tables_horaire_hiver[$horaire->jour_en_semaine][$horaire->time_hiver_debut]['time_debut'] = $horaire->time_hiver_debut;
            $tables_horaire_hiver[$horaire->jour_en_semaine][$horaire->time_hiver_debut]['time_fin'] = $horaire->time_hiver_fin;



        }

        /**** parti recuperer les salles ***/
        $salles = Salle::all();

        /**** parti recuperer les classe existent ***/
        $classes_currents = Calsse::where('formation_id', $formation->id )->get();
        $classes = array();
        foreach ($classes_currents as $classe_current) {
            $classes[$classe_current->horaire_id.':'.$classe_current->salle_id] = $classe_current;
        }

        return view('classes.determine', compact('tables_horaire_hiver', 'tables_horaire_ete', 'tables_horaire_ramadan', 'jours', 'time_initial', 'time_final', 'classes', 'formation', 'salles'));
    }

    public function creat($id_formation, $id_horaire, $id_salle) {
        $formation = Formation::find($id_formation);
        $horaire = Horaire::find($id_horaire);
        $salle = Salle::find($id_salle);

        $groupes = Groupe::all();

        $goupes_existent= array();
        foreach ($formation->classes as $classe) {
            $goupes_existent[] = $classe->groupe_id;
        }

        return view('classes.add', compact('formation','horaire', 'salle', 'groupes', 'goupes_existent'));
    }

    public function store(Request $request) {
        $this->validate($request, array(
            'formation_id' => 'required|numeric|min:1',
            'horaire_id' => 'required|numeric|min:1',
            'salle_id' => 'required|numeric|min:1',
            'groupe_id' => 'required|numeric|min:1|unique:calsses,groupe_id,NULL,id,formation_id,'.$request->formation_id,
            'max_childrens' => 'required|numeric|min:1'
        ));

        $classe = new Calsse;
        $classe->formation_id = $request->formation_id;
        $classe->horaire_id = $request->horaire_id;
        $classe->salle_id = $request->salle_id;
        $classe->groupe_id = $request->groupe_id;
        $classe->max_childrens = $request->max_childrens;
        $classe->save();

        Session::flash('success', 'La classe de cette formation a été ajoutée!');

        return redirect()->route('classes.determine', $classe->formation_id);
    }

    public function edit($id_classe) {
        $classe = Calsse::find($id_classe);

        $groupes = Groupe::all();

        $goupes_existent= array();
        foreach ($classe->formation->classes as $classe_existe) {
            if($classe_existe->id != $classe->id) {
                $goupes_existent[] = $classe_existe->groupe_id;
            }

        }

        return view('classes.edit', compact('classe', 'groupes', 'goupes_existent'));
    }



    public function update(Request $request, $id) {

        $classe = Calsse::find($id);

        $this->validate($request, array(
            'groupe_id' => 'required|numeric|min:1|unique:calsses,groupe_id,'.$classe->id.',id,formation_id,'.$classe->formation_id,
            'max_childrens' => 'required|numeric|min:1'
        ));

        $classe->groupe_id = $request->groupe_id;
        $classe->max_childrens = $request->max_childrens;
        $classe->save();

        Session::flash('success', 'La classe a été modifiée!');

        return redirect()->route('classes.determine', $classe->formation_id);






    }

    public function destroy($id)
    {
        $classe = Calsse::find($id);

        $formation_id = $classe->formation_id;
        $nom_groupe = $classe->groupe->nom;

        $classe->delete();

        Session::flash('success', 'La classe ' . $nom_groupe . ' a été supprimer!');

        return redirect()->route('classes.determine', $formation_id);
    }

}
