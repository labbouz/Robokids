<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Formation;
use App\Horaire;
use App\Calsse;
use App\Groupe;
use App\Niveau;


use App\Salle;

use Session;

class InscriptionController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create( $id_formation )
    {
        /*
         * Parti récupération les horaires générales
         */
        $horaires = Horaire::all();

        $jours =array(
            6 => 'SAM',
            7 => 'DIM',
            1 => 'LUN',
            2 => 'MAR',
            3 => 'MER',
            4 => 'JEU',
            5 => 'VEN'
        );

        $time_initial = '08:30';
        $time_final = '19:00';



        foreach ($horaires as $horaire) {


            $defirence_hiver = (strtotime($horaire->time_hiver_fin) - strtotime($horaire->time_hiver_debut)) / (2*60*60);

            $tables_horaire_hiver[$horaire->jour_en_semaine][$horaire->time_hiver_debut]['rows'] = $defirence_hiver;
            $tables_horaire_hiver[$horaire->jour_en_semaine][$horaire->time_hiver_debut]['id'] = $horaire->id;
            $tables_horaire_hiver[$horaire->jour_en_semaine][$horaire->time_hiver_debut]['nom'] = $horaire->nom;
            $tables_horaire_hiver[$horaire->jour_en_semaine][$horaire->time_hiver_debut]['code'] = $horaire->code;
            $tables_horaire_hiver[$horaire->jour_en_semaine][$horaire->time_hiver_debut]['time_debut'] = $horaire->time_hiver_debut;
            $tables_horaire_hiver[$horaire->jour_en_semaine][$horaire->time_hiver_debut]['time_fin'] = $horaire->time_hiver_fin;


        }

        $formation = Formation::find($id_formation);

        /**** parti recuperer les salles ***/
        $salles = Salle::all();

        /**** parti recuperer les classe existent ***/
        $classes_currents = Calsse::where('formation_id', $formation->id )->get();
        $classes = array();
        foreach ($classes_currents as $classe_current) {
            $classes[$classe_current->horaire_id.':'.$classe_current->salle_id] = $classe_current;
        }

        $niveaux = Niveau::all()->sortBy("rang")->sortBy("module_id");

        $genres = array();
        $genres['garcon'] = 'Garçon';
        $genres['fille'] = 'Fille';

        return view('inscriptions.index', compact('tables_horaire_hiver', 'tables_horaire_ete', 'tables_horaire_ramadan', 'jours', 'time_initial', 'time_final', 'salles', 'classes', 'formation', 'niveaux', 'genres'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function listInscrits($id_formation)
    {
        $formation = Formation::find($id_formation);

        return view('abonnements.index', compact('formation'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
