<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Enfant extends Model
{

    protected $table = 'enfants';

    protected $fillable = ['nom', 'prenom', 'date_naissance', 'genre', 'ecole', 'photo'
        , 'adresse', 'ville', 'code_postal'
        , 'nom_pere', 'prenom_pere', 'telephone_pere', 'email_pere'
        , 'nom_mere', 'prenom_mere', 'telephone_mere', 'email_mere'];

    public function abonnements()
    {
        return $this->hasMany('\App\Abonnement');
    }

}
