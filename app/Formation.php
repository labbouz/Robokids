<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Formation extends Model
{
    protected $table = 'formations';

    protected $fillable = ['nom', 'date_debut', 'date_fin', 'active', 'frais_inscription', 'frais_session', 'pourcentage_double', 'pourcentage_triple'];

    public function classes()
    {
        return $this->hasMany('\App\Calsse');
    }

    public function abonnements()
    {
        return $this->hasMany('\App\Abonnement');
    }
}
