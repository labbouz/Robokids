<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Groupe extends Model
{
    protected $table = 'groupes';

    protected $fillable = ['nom', 'niveau_id'];

    public function niveau()
    {
        return $this->belongsTo('\App\Niveau');
    }

    public function classes()
    {
        return $this->hasMany('\App\Calsse');
    }
}
