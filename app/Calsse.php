<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Calsse extends Model
{
    protected $table = 'calsses';

    protected $fillable = ['formation_id', 'horaire_id', 'salle_id', 'groupe_id', 'max_childrens'];

    public function formation()
    {
        return $this->belongsTo('\App\Formation');
    }

    public function salle()
    {
        return $this->belongsTo('\App\Salle');
    }

    public function horaire()
    {
        return $this->belongsTo('\App\Horaire');
    }

    public function groupe()
    {
        return $this->belongsTo('\App\Groupe');
    }

    public function abonnements()
    {
        return $this->belongsToMany('App\Abonnement')->withTimestamps();
    }

    public function abonnements_active()
    {
        return $this->belongsToMany('App\Abonnement')->wherePivot('active', true)->withTimestamps();
    }

}
