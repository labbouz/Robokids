<?php

namespace App\Widgets;

use Arrilot\Widgets\AbstractWidget;

use App\Formation;


class FormationMenus extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {

        $formation_active = Formation::where('active', true)->first();

        return view('widgets.formation_menus', [
            'config' => $this->config,
            'formation_active' => $formation_active,
        ]);
    }
}
